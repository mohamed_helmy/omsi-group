# -*- coding: utf-8 -*-
{
    'name': "OMSI Quotation",

    'summary': """
        Pricing list """,

    'description': """
        To Pricing list
    """,
    'author': "Beshoy Wageh",
    'website': "https://www.linkedin.com/in/beshoy-wageh-701ba7116/",
    'version': '0.1',
    'depends': ['base','stock','sale_management'],
    'data': [
        'security/ir.model.access.csv',
        'views/quotation_view.xml',
        'views/system_view.xml',
        'views/profile_type_view.xml',
        'views/fly_accessor_view.xml',
        'views/quotation_template_view.xml',
        'views/accessories_view.xml',
        'views/sectors_view.xml',
        'views/paints_view.xml',
        'views/glass_view.xml',
        'views/fly_screen_view.xml',
        'views/glass_color_view.xml',
        'views/glass_price_view.xml',
        'reports/quotation_report.xml',

    ],

}
