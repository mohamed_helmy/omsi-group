from odoo import models, fields, api


class Sectors(models.Model):
    _name = 'sectors'
    _rec_name = 'sector_name'

    sector_name = fields.Char('Sectors Name')
    name = fields.Many2one('system.template')
    profile_type_ids = fields.Many2one('profile.type', domain="[('system_ids','=', name)]")
    margin = fields.Float('Margin', default=1.90)
    quot_template = fields.Many2one('quotation.template', string='Quotation Template')
    type = fields.Selection([
        ('two_dalfa', ' جرار 2 ضلفه '),
        ('tree_dalfa', ' جرار 3 ضلفة '),
        ('four_dalfa', ' جرار 4 ضلفة ')],
        string='Design', required=True, default="two_dalfa")
    profile_types = fields.Selection([
        ('jumbo', ' جامبو '),
        ('tango', ' تانجو '),
        ('ps_9600', ' PS 9600 ')
    ], string='Profile', required=True, default="jumbo")
    kilo_rate = fields.Float('Kilo Rate', default=58.71)
    w = fields.Float('W', related='name.w', readonly=False)
    h = fields.Float('H', related='name.h', readonly=False)
    total = fields.Float('Total', compute='get_total', digits=(16, 3))

    ########################################################################################################################

    ###################################
    ###########    Jumbo   ############
    ###################################

    ########################################################################################################################

    # line  1  ==>   FRAME PROFILE J 1038
    desc1 = fields.Char('Description', default='FRAME PROFILE J 1038')
    desc1_qty = fields.Float('Qty', compute="get_qty1", digits=(16, 3))
    desc1_purchase = fields.Float('Purchase', compute='get_desc1_purchase', digits=(16, 3))
    desc1_sale = fields.Float('Sale', compute='get_desc1_sale', digits=(16, 3))
    desc1_weight = fields.Float('Weight', default=1.824, digits=(16, 3))
    desc1_total = fields.Float('Total', compute='get_desc1_total', digits=(16, 3))

    @api.depends('type')
    def get_qty1(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc1_qty = 0.0
            else:
                rec.desc1_qty = ((rec.w * 2.0) + (rec.h * 2.0)) * 1.10

    @api.depends('kilo_rate', 'desc1_weight')
    def get_desc1_purchase(self):
        for rec in self:
            rec.desc1_purchase = rec.desc1_weight * rec.kilo_rate

    @api.depends('margin', 'desc1_purchase')
    def get_desc1_sale(self):
        for rec in self:
            rec.desc1_sale = rec.margin * rec.desc1_purchase

    @api.depends('desc1_qty', 'desc1_sale')
    def get_desc1_total(self):
        for rec in self:
            rec.desc1_total = rec.desc1_qty * rec.desc1_sale

    ########################################################################################################################
    # line  2  ==>    FRAME PROFILE J 1039
    desc2 = fields.Char('Description', default=' FRAME PROFILE J 1039')
    desc2_qty = fields.Float('Qty', compute="get_qty2", digits=(16, 3))
    desc2_purchase = fields.Float('Purchase', compute='get_desc2_purchase', digits=(16, 3))
    desc2_sale = fields.Float('Sale', compute='get_desc2_sale', digits=(16, 3))
    desc2_weight = fields.Float('Weight', digits=(16, 3))
    desc2_total = fields.Float('Total', compute='get_desc2_total', digits=(16, 3))

    @api.depends('type')
    def get_qty2(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc2_qty = ((rec.w * 2.0) + (rec.h * 2.0)) * 1.10
            else:
                rec.desc2_qty = 0.0

    @api.depends('kilo_rate', 'desc2_weight')
    def get_desc2_purchase(self):
        for rec in self:
            rec.desc2_purchase = rec.desc2_weight * rec.kilo_rate

    @api.depends('margin', 'desc2_purchase')
    def get_desc2_sale(self):
        for rec in self:
            rec.desc2_sale = rec.margin * rec.desc2_purchase

    @api.depends('desc2_qty', 'desc2_sale')
    def get_desc2_total(self):
        for rec in self:
            rec.desc2_total = rec.desc2_qty * rec.desc2_sale

    ########################################################################################################################
    # line  3  ==>     SASH J 1040
    desc3 = fields.Char('Description', default='SASH J 1040')
    desc3_qty = fields.Float('Qty', compute="get_qty3", digits=(16, 3))
    desc3_purchase = fields.Float('Purchase', compute='get_desc3_purchase', digits=(16, 3))
    desc3_sale = fields.Float('Sale', compute='get_desc3_sale', digits=(16, 3))
    desc3_weight = fields.Float('Weight', default=1.457, digits=(16, 3))
    desc3_total = fields.Float('Total', compute='get_desc3_total', digits=(16, 3))

    @api.depends('type')
    def get_qty3(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc3_qty = ((rec.w * 2.0) + (rec.h * 6.0)) * 1.10
            elif rec.type in ('four_dalfa'):
                rec.desc3_qty = ((rec.w * 2.0) + (rec.h * 8.0)) * 1.10
            else:
                rec.desc3_qty = ((rec.w * 2.0) + (rec.h * 4.0)) * 1.10

    @api.depends('kilo_rate', 'desc3_weight')
    def get_desc3_purchase(self):
        for rec in self:
            rec.desc3_purchase = rec.desc3_weight * rec.kilo_rate

    @api.depends('margin', 'desc3_purchase')
    def get_desc3_sale(self):
        for rec in self:
            rec.desc3_sale = rec.margin * rec.desc3_purchase

    @api.depends('desc3_qty', 'desc3_sale')
    def get_desc3_total(self):
        for rec in self:
            rec.desc3_total = rec.desc3_qty * rec.desc3_sale

    ########################################################################################################################
    # line  4  ==>      Aluminum track J 1091
    desc4 = fields.Char('Description', default='Aluminum track J 1091')
    desc4_qty = fields.Float('Qty', compute="get_qty4", digits=(16, 3))
    desc4_purchase = fields.Float('Purchase', compute='get_desc4_purchase', digits=(16, 3))
    desc4_sale = fields.Float('Sale', compute='get_desc4_sale', digits=(16, 3))
    desc4_weight = fields.Float('Weight', default=0.144, digits=(16, 3))
    desc4_total = fields.Float('Total', compute='get_desc4_total', digits=(16, 3))

    @api.depends('type')
    def get_qty4(self):
        for rec in self:
            rec.desc4_qty = (rec.w * 2.0) * 1.10

    @api.depends('kilo_rate', 'desc4_weight')
    def get_desc4_purchase(self):
        for rec in self:
            rec.desc4_purchase = rec.desc4_weight * rec.kilo_rate

    @api.depends('margin', 'desc4_purchase')
    def get_desc4_sale(self):
        for rec in self:
            rec.desc4_sale = rec.margin * rec.desc4_purchase

    @api.depends('desc4_qty', 'desc4_sale')
    def get_desc4_total(self):
        for rec in self:
            rec.desc4_total = rec.desc4_qty * rec.desc4_sale

    ########################################################################################################################

    # line  5  ==>       OVERLAPPING (Boumbeh 7cm) SH 8086.2
    desc5 = fields.Char('Description', default='OVERLAPPING (Boumbeh 7cm) SH 8086.2')
    desc5_qty = fields.Float('Qty', compute="get_qty5", digits=(16, 3))
    desc5_purchase = fields.Float('Purchase', compute='get_desc5_purchase', digits=(16, 3))
    desc5_sale = fields.Float('Sale', compute='get_desc5_sale', digits=(16, 3))
    desc5_weight = fields.Float('Weight', default=0.487, digits=(16, 3))
    desc5_total = fields.Float('Total', compute='get_desc5_total', digits=(16, 3))

    @api.depends('type')
    def get_qty5(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc5_qty = 0.0
            else:
                rec.desc5_qty = ((rec.w * 2.0) + (rec.h * 2.0)) * 1.10

    @api.depends('kilo_rate', 'desc5_weight')
    def get_desc5_purchase(self):
        for rec in self:
            rec.desc5_purchase = rec.desc5_weight * rec.kilo_rate

    @api.depends('margin', 'desc5_purchase')
    def get_desc5_sale(self):
        for rec in self:
            rec.desc5_sale = rec.margin * rec.desc5_purchase

    @api.depends('desc5_qty', 'desc5_sale')
    def get_desc5_total(self):
        for rec in self:
            rec.desc5_total = rec.desc5_qty * rec.desc5_sale

    ########################################################################################################################

    # line  6  ==>        INTER LOOK J 1050

    desc6 = fields.Char('Description', default='INTER LOOK J 1050')
    desc6_qty = fields.Float('Qty', compute="get_qty6", digits=(16, 3))
    desc6_purchase = fields.Float('Purchase', compute='get_desc6_purchase', digits=(16, 3))
    desc6_sale = fields.Float('Sale', compute='get_desc6_sale', digits=(16, 3))
    desc6_weight = fields.Float('Weight', default=0.671, digits=(16, 3))
    desc6_total = fields.Float('Total', compute='get_desc6_total', digits=(16, 3))

    @api.depends('type')
    def get_qty6(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc6_qty = (rec.h * 2.0) * 1.10
            else:
                rec.desc6_qty = (rec.h * 4.0) * 1.10

    @api.depends('kilo_rate', 'desc6_weight')
    def get_desc6_purchase(self):
        for rec in self:
            rec.desc6_purchase = rec.desc6_weight * rec.kilo_rate

    @api.depends('margin', 'desc6_purchase')
    def get_desc6_sale(self):
        for rec in self:
            rec.desc6_sale = rec.margin * rec.desc6_purchase

    @api.depends('desc6_qty', 'desc6_sale')
    def get_desc6_total(self):
        for rec in self:
            rec.desc6_total = rec.desc6_qty * rec.desc6_sale

    ########################################################################################################################

    # line  7  ==>         Central Inseat J 1095

    desc7 = fields.Char('Description', default='Central Inseat J 1095')
    desc7_qty = fields.Float('Qty', compute="get_qty7", digits=(16, 3))
    desc7_purchase = fields.Float('Purchase', compute='get_desc7_purchase', digits=(16, 3))
    desc7_sale = fields.Float('Sale', compute='get_desc7_sale', digits=(16, 3))
    desc7_weight = fields.Float('Weight', digits=(16, 3))
    desc7_total = fields.Float('Total', compute='get_desc7_total', digits=(16, 3))

    @api.depends('type')
    def get_qty7(self):
        for rec in self:
            if rec.type in ('four_dalfa'):
                rec.desc7_qty = (rec.w * 1.0) * 1.10
            else:
                rec.desc7_qty = 0.0

    @api.depends('kilo_rate', 'desc7_weight')
    def get_desc7_purchase(self):
        for rec in self:
            rec.desc7_purchase = rec.desc7_weight * rec.kilo_rate

    @api.depends('margin', 'desc7_purchase')
    def get_desc7_sale(self):
        for rec in self:
            rec.desc7_sale = rec.margin * rec.desc7_purchase

    @api.depends('desc7_qty', 'desc7_sale')
    def get_desc7_total(self):
        for rec in self:
            rec.desc7_total = rec.desc7_qty * rec.desc7_sale

    ########################################################################################################################

    ###################################
    ###########    Tango   ############
    ###################################

    ########################################################################################################################

    # line  8  ==>    FRAME PROFILE TG 6020
    desc8 = fields.Char('Description', default='FRAME PROFILE TG 6020')
    desc8_qty = fields.Float('Qty', compute="get_qty8", digits=(16, 3))
    desc8_purchase = fields.Float('Purchase', compute='get_desc8_purchase', digits=(16, 3))
    desc8_sale = fields.Float('Sale', compute='get_desc8_sale', digits=(16, 3))
    desc8_weight = fields.Float('Weight', default=1.691, digits=(16, 3))
    desc8_total = fields.Float('Total', compute='get_desc8_total', digits=(16, 3))

    @api.depends('type')
    def get_qty8(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc8_qty = 0.0
            else:
                rec.desc8_qty = ((rec.w * 2.0) + (rec.h * 2.0)) * 1.10

    @api.depends('kilo_rate', 'desc8_weight')
    def get_desc8_purchase(self):
        for rec in self:
            rec.desc8_purchase = rec.desc8_weight * rec.kilo_rate

    @api.depends('margin', 'desc8_purchase')
    def get_desc8_sale(self):
        for rec in self:
            rec.desc8_sale = rec.margin * rec.desc8_purchase

    @api.depends('desc8_qty', 'desc8_sale')
    def get_desc8_total(self):
        for rec in self:
            rec.desc8_total = rec.desc8_qty * rec.desc8_sale

    ########################################################################################################################

    # line  9  ==>     FRAME PROFILE TG 6038
    desc9 = fields.Char('Description', default='FRAME PROFILE TG 6038')
    desc9_qty = fields.Float('Qty', compute="get_qty9", digits=(16, 3))
    desc9_purchase = fields.Float('Purchase', compute='get_desc9_purchase', digits=(16, 3))
    desc9_sale = fields.Float('Sale', compute='get_desc9_sale', digits=(16, 3))
    desc9_weight = fields.Float('Weight', default=1.778, digits=(16, 3))
    desc9_total = fields.Float('Total', compute='get_desc9_total', digits=(16, 3))

    @api.depends('type')
    def get_qty9(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc9_qty = ((rec.w * 2.0) + (rec.h * 2.0)) * 1.10
            else:
                rec.desc9_qty = 0.0

    @api.depends('kilo_rate', 'desc9_weight')
    def get_desc9_purchase(self):
        for rec in self:
            rec.desc9_purchase = rec.desc9_weight * rec.kilo_rate

    @api.depends('margin', 'desc9_purchase')
    def get_desc9_sale(self):
        for rec in self:
            rec.desc9_sale = rec.margin * rec.desc9_purchase

    @api.depends('desc9_qty', 'desc9_sale')
    def get_desc9_total(self):
        for rec in self:
            rec.desc9_total = rec.desc9_qty * rec.desc9_sale

    ########################################################################################################################

    # line  10  ==>      SASH TG 6040
    desc10 = fields.Char('Description', default='SASH TG 6040')
    desc10_qty = fields.Float('Qty', compute="get_qty10", digits=(16, 3))
    desc10_purchase = fields.Float('Purchase', compute='get_desc10_purchase', digits=(16, 3))
    desc10_sale = fields.Float('Sale', compute='get_desc10_sale', digits=(16, 3))
    desc10_weight = fields.Float('Weight', default=0.970, digits=(16, 3))
    desc10_total = fields.Float('Total', compute='get_desc10_total', digits=(16, 3))

    @api.depends('type')
    def get_qty10(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc10_qty = ((rec.w * 2.0) + (rec.h * 6.0)) * 1.10
            elif rec.type in ('four_dalfa'):
                rec.desc10_qty = ((rec.w * 2.0) + (rec.h * 8.0)) * 1.10
            else:
                rec.desc10_qty = ((rec.w * 2.0) + (rec.h * 4.0)) * 1.10

    @api.depends('kilo_rate', 'desc10_weight')
    def get_desc10_purchase(self):
        for rec in self:
            rec.desc10_purchase = rec.desc10_weight * rec.kilo_rate

    @api.depends('margin', 'desc10_purchase')
    def get_desc10_sale(self):
        for rec in self:
            rec.desc10_sale = rec.margin * rec.desc10_purchase

    @api.depends('desc10_qty', 'desc10_sale')
    def get_desc10_total(self):
        for rec in self:
            rec.desc10_total = rec.desc10_qty * rec.desc10_sale

    ########################################################################################################################

    # line  11  ==>      OVERLAPPING (Boumbeh 5.4cm) SH 8085.2
    desc11 = fields.Char('Description', default='OVERLAPPING (Boumbeh 5.4cm) SH 8085.2')
    desc11_qty = fields.Float('Qty', compute="get_qty11", digits=(16, 3))
    desc11_purchase = fields.Float('Purchase', compute='get_desc11_purchase', digits=(16, 3))
    desc11_sale = fields.Float('Sale', compute='get_desc11_sale', digits=(16, 3))
    desc11_weight = fields.Float('Weight', default=0.578, digits=(16, 3))
    desc11_total = fields.Float('Total', compute='get_desc11_total', digits=(16, 3))

    @api.depends('type')
    def get_qty11(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc11_qty = ((rec.w * 2.0) + (rec.h * 2.0)) * 1.10
            else:
                rec.desc11_qty = 0.0

    @api.depends('kilo_rate', 'desc11_weight')
    def get_desc11_purchase(self):
        for rec in self:
            rec.desc11_purchase = rec.desc11_weight * rec.kilo_rate

    @api.depends('margin', 'desc11_purchase')
    def get_desc11_sale(self):
        for rec in self:
            rec.desc11_sale = rec.margin * rec.desc11_purchase

    @api.depends('desc11_qty', 'desc11_sale')
    def get_desc11_total(self):
        for rec in self:
            rec.desc11_total = rec.desc11_qty * rec.desc11_sale

    ########################################################################################################################

    # line  12  ==>       INTER LOOK TG 6050
    desc12 = fields.Char('Description', default='INTER LOOK TG 6050')
    desc12_qty = fields.Float('Qty', compute="get_qty12", digits=(16, 3))
    desc12_purchase = fields.Float('Purchase', compute='get_desc12_purchase', digits=(16, 3))
    desc12_sale = fields.Float('Sale', compute='get_desc12_sale', digits=(16, 3))
    desc12_weight = fields.Float('Weight', default=0.322, digits=(16, 3))
    desc12_total = fields.Float('Total', compute='get_desc12_total', digits=(16, 3))

    @api.depends('type')
    def get_qty12(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc12_qty = (rec.h * 2.0) * 1.10
            else:
                rec.desc12_qty = (rec.h * 4.0) * 1.10

    @api.depends('kilo_rate', 'desc12_weight')
    def get_desc12_purchase(self):
        for rec in self:
            rec.desc12_purchase = rec.desc12_weight * rec.kilo_rate

    @api.depends('margin', 'desc12_purchase')
    def get_desc12_sale(self):
        for rec in self:
            rec.desc12_sale = rec.margin * rec.desc12_purchase

    @api.depends('desc12_qty', 'desc12_sale')
    def get_desc12_total(self):
        for rec in self:
            rec.desc12_total = rec.desc12_qty * rec.desc12_sale

    ########################################################################################################################

    # line  13  ==>      Central Inseat TG 6095
    desc13 = fields.Char('Description', default='Central Inseat TG 6095')
    desc13_qty = fields.Float('Qty', compute="get_qty13", digits=(16, 3))
    desc13_purchase = fields.Float('Purchase', compute='get_desc13_purchase', digits=(16, 3))
    desc13_sale = fields.Float('Sale', compute='get_desc13_sale', digits=(16, 3))
    desc13_weight = fields.Float('Weight', default=0.412, digits=(16, 3))
    desc13_total = fields.Float('Total', compute='get_desc13_total', digits=(16, 3))

    @api.depends('type')
    def get_qty13(self):
        for rec in self:
            if rec.type in ('four_dalfa'):
                rec.desc13_qty = rec.h * 1.10
            else:
                rec.desc13_qty = 0.0

    @api.depends('kilo_rate', 'desc13_weight')
    def get_desc13_purchase(self):
        for rec in self:
            rec.desc13_purchase = rec.desc13_weight * rec.kilo_rate

    @api.depends('margin', 'desc13_purchase')
    def get_desc13_sale(self):
        for rec in self:
            rec.desc13_sale = rec.margin * rec.desc13_purchase

    @api.depends('desc13_qty', 'desc13_sale')
    def get_desc13_total(self):
        for rec in self:
            rec.desc13_total = rec.desc13_qty * rec.desc13_sale

    ########################################################################################################################

    #####################################
    ###########    PS 9600   ############
    #####################################

    ########################################################################################################################

    # line  14  ==>     FRAME PROFILE  PS 9601
    desc14 = fields.Char('Description', default=' FRAME PROFILE  PS 9601 ')
    desc14_qty = fields.Float('Qty', compute="get_qty14", digits=(16, 3))
    desc14_purchase = fields.Float('Purchase', compute='get_desc14_purchase', digits=(16, 3))
    desc14_sale = fields.Float('Sale', compute='get_desc14_sale', digits=(16, 3))
    desc14_weight = fields.Float('Weight', digits=(16, 3), default=1.840)
    desc14_total = fields.Float('Total', compute='get_desc14_total', digits=(16, 3))

    @api.depends('type')
    def get_qty14(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc14_qty = 0.0
            else:
                rec.desc14_qty = ((rec.w * 2.0) + (rec.h * 2.0)) * 1.10

    @api.depends('kilo_rate', 'desc14_weight')
    def get_desc14_purchase(self):
        for rec in self:
            rec.desc14_purchase = rec.desc14_weight * rec.kilo_rate

    @api.depends('margin', 'desc14_purchase')
    def get_desc14_sale(self):
        for rec in self:
            rec.desc14_sale = rec.margin * rec.desc14_purchase

    @api.depends('desc14_qty', 'desc14_sale')
    def get_desc14_total(self):
        for rec in self:
            rec.desc14_total = rec.desc14_qty * rec.desc14_sale

########################################################################################################################

    # line  15  ==>    FRAME PROFILE  PS 9020
    desc15 = fields.Char('Description', default=' FRAME PROFILE  PS 9020 ')
    desc15_qty = fields.Float('Qty', compute="get_qty15", digits=(16, 3))
    desc15_purchase = fields.Float('Purchase', compute='get_desc15_purchase', digits=(16, 3))
    desc15_sale = fields.Float('Sale', compute='get_desc15_sale', digits=(16, 3))
    desc15_weight = fields.Float('Weight', digits=(16, 3), default=2.00)
    desc15_total = fields.Float('Total', compute='get_desc15_total', digits=(16, 3))

    @api.depends('type')
    def get_qty15(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc15_qty = ((rec.w * 2.0) + (rec.h * 2.0)) * 1.10
            else:
                rec.desc15_qty = 0.0

    @api.depends('kilo_rate', 'desc15_weight')
    def get_desc15_purchase(self):
        for rec in self:
            rec.desc15_purchase = rec.desc15_weight * rec.kilo_rate

    @api.depends('margin', 'desc15_purchase')
    def get_desc15_sale(self):
        for rec in self:
            rec.desc15_sale = rec.margin * rec.desc15_purchase

    @api.depends('desc15_qty', 'desc15_sale')
    def get_desc15_total(self):
        for rec in self:
            rec.desc15_total = rec.desc15_qty * rec.desc15_sale

########################################################################################################################

    # line  16  ==>     SASH PS 9602
    desc16 = fields.Char('Description', default='SASH PS 9602')
    desc16_qty = fields.Float('Qty', compute="get_qty16", digits=(16, 3))
    desc16_purchase = fields.Float('Purchase', compute='get_desc16_purchase', digits=(16, 3))
    desc16_sale = fields.Float('Sale', compute='get_desc16_sale', digits=(16, 3))
    desc16_weight = fields.Float('Weight', default=1.104, digits=(16, 3))
    desc16_total = fields.Float('Total', compute='get_desc16_total', digits=(16, 3))

    @api.depends('type')
    def get_qty16(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc16_qty = ((rec.w * 2.0) + (rec.h * 6.0)) * 1.10
            elif rec.type in ('four_dalfa'):
                rec.desc16_qty = ((rec.w * 2.0) + (rec.h * 8.0)) * 1.10
            else:
                rec.desc16_qty = ((rec.w * 2.0) + (rec.h * 4.0)) * 1.10

    @api.depends('kilo_rate', 'desc16_weight')
    def get_desc16_purchase(self):
        for rec in self:
            rec.desc16_purchase = rec.desc16_weight * rec.kilo_rate

    @api.depends('margin', 'desc16_purchase')
    def get_desc16_sale(self):
        for rec in self:
            rec.desc16_sale = rec.margin * rec.desc16_purchase

    @api.depends('desc16_qty', 'desc16_sale')
    def get_desc16_total(self):
        for rec in self:
            rec.desc16_total = rec.desc16_qty * rec.desc16_sale


########################################################################################################################

    # line  17  ==>      OVERLAPPING (Boumbeh 6.8cm) PS 9623
    desc17 = fields.Char('Description', default='OVERLAPPING (Boumbeh 6.8cm) PS 9623')
    desc17_qty = fields.Float('Qty', compute="get_qty17", digits=(16, 3))
    desc17_purchase = fields.Float('Purchase', compute='get_desc17_purchase', digits=(16, 3))
    desc17_sale = fields.Float('Sale', compute='get_desc17_sale', digits=(16, 3))
    desc17_weight = fields.Float('Weight', default=0.430, digits=(16, 3))
    desc17_total = fields.Float('Total', compute='get_desc17_total', digits=(16, 3))

    @api.depends('type')
    def get_qty17(self):
        for rec in self:
            rec.desc17_qty = ((rec.w * 2.0) + (rec.h * 2.0)) * 1.10

    @api.depends('kilo_rate', 'desc17_weight')
    def get_desc17_purchase(self):
        for rec in self:
            rec.desc17_purchase = rec.desc17_weight * rec.kilo_rate

    @api.depends('margin', 'desc17_purchase')
    def get_desc17_sale(self):
        for rec in self:
            rec.desc17_sale = rec.margin * rec.desc17_purchase

    @api.depends('desc17_qty', 'desc17_sale')
    def get_desc17_total(self):
        for rec in self:
            rec.desc17_total = rec.desc17_qty * rec.desc17_sale


########################################################################################################################

    # line  18  ==>   INTER LOOK PS 9603
    desc18 = fields.Char('Description', default='INTER LOOK PS 9603')
    desc18_qty = fields.Float('Qty', compute="get_qty18", digits=(16, 3))
    desc18_purchase = fields.Float('Purchase', compute='get_desc18_purchase', digits=(16, 3))
    desc18_sale = fields.Float('Sale', compute='get_desc18_sale', digits=(16, 3))
    desc18_weight = fields.Float('Weight', default=0.580, digits=(16, 3))
    desc18_total = fields.Float('Total', compute='get_desc18_total', digits=(16, 3))

    @api.depends('type')
    def get_qty18(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc18_qty = (rec.h * 2.0) * 1.10
            else:
                rec.desc18_qty = (rec.h * 4.0) * 1.10

    @api.depends('kilo_rate', 'desc18_weight')
    def get_desc18_purchase(self):
        for rec in self:
            rec.desc18_purchase = rec.desc18_weight * rec.kilo_rate

    @api.depends('margin', 'desc18_purchase')
    def get_desc18_sale(self):
        for rec in self:
            rec.desc18_sale = rec.margin * rec.desc18_purchase

    @api.depends('desc16_qty', 'desc18_sale')
    def get_desc18_total(self):
        for rec in self:
            rec.desc18_total = rec.desc18_qty * rec.desc18_sale

########################################################################################################################

    # line 19  ==>   Face to Face PS 9605

    desc19 = fields.Char('Description', default='Face to Face PS 9605')
    desc19_qty = fields.Float('Qty', compute="get_qty19", digits=(16, 3))
    desc19_purchase = fields.Float('Purchase', compute='get_desc19_purchase', digits=(16, 3))
    desc19_sale = fields.Float('Sale', compute='get_desc19_sale', digits=(16, 3))
    desc19_weight = fields.Float('Weight', digits=(16, 3), default=0.385)
    desc19_total = fields.Float('Total', compute='get_desc19_total', digits=(16, 3))

    @api.depends('type')
    def get_qty19(self):
        for rec in self:
            if rec.type in ('four_dalfa'):
                rec.desc19_qty = (rec.h * 1.0) * 1.10
            else:
                rec.desc19_qty = 0.0

    @api.depends('kilo_rate', 'desc19_weight')
    def get_desc19_purchase(self):
        for rec in self:
            rec.desc19_purchase = rec.desc19_weight * rec.kilo_rate

    @api.depends('margin', 'desc19_purchase')
    def get_desc19_sale(self):
        for rec in self:
            rec.desc19_sale = rec.margin * rec.desc19_purchase

    @api.depends('desc15_qty', 'desc19_sale')
    def get_desc19_total(self):
        for rec in self:
            rec.desc19_total = rec.desc19_qty * rec.desc19_sale

########################################################################################################################

    @api.depends('profile_types', 'desc1_total', 'desc2_total', 'desc3_total', 'desc4_total', 'desc5_total',
                 'desc6_total', 'desc7_total', 'desc8_total', 'desc9_total', 'desc10_total', 'desc11_total',
                 'desc12_total', 'desc13_total', 'desc14_total', 'desc15_total', 'desc16_total','desc17_total',
                 'desc18_total', 'desc19_total')
    def get_total(self):
        for rec in self:
            if rec.profile_types == 'jumbo':
                rec.total = rec.desc1_total + rec.desc2_total + rec.desc3_total + rec.desc4_total + rec.desc5_total + rec.desc6_total + rec.desc7_total
            elif rec.profile_types == 'tango':
                rec.total = rec.desc8_total + rec.desc9_total + rec.desc10_total + rec.desc11_total + rec.desc12_total + rec.desc13_total
            elif rec.profile_types == 'ps_9600':
                rec.total = rec.desc14_total + rec.desc15_total + rec.desc16_total + rec.desc17_total + rec.desc18_total + rec.desc19_total
            else:
                rec.total = 0.0

########################################################################################################################
