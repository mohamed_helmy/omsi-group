
from odoo import models, fields, api
import datetime

class Quotation(models.Model):
    _name = 'omsi.quotaion'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']

    name = fields.Char(index=True, readonly=True)
    customer = fields.Many2one('res.partner', required=True)
    quot_date = fields.Datetime(default= datetime.datetime.today(), required=True, string='Quotation Date')
    quot_template = fields.Many2many('quotation.template', string='Quotation Template')
    notes = fields.Text()
    total_all = fields.Float(string='Total',defualt=0.0, compute='get_total_all')
    quotaion_id = fields.One2many('omsi.quotaion.line','quotaion_ids')

    @api.model
    def create(self, vals):
        sequence = self.env['ir.sequence'].next_by_code('sequence_generaion')
        vals['name'] = sequence
        result = super(Quotation, self).create(vals)
        return result

    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        return super(Quotation, self).copy(default=default)


    # This function using to get quotation template from quotation.template model
    def _compute_line_data_for_template_change(self, line):
        return {
            'code': line.code,
            'unit_loc': line.unit_loc,
            'system_id': line.system_id,
            'profile_type_ids': line.profile_type_ids,
            'profile_color_ids': line.profile_color_ids,
            'dim_w': line.dim_w,
            'dim_h': line.dim_h,
            'area': line.area,
            'qty': line.qty,
            'total': line.total,
            'extra': line.extra,
            # 'vertical': line.vertical,
            # 'horizontal': line.horizontal,
            'glass_type': line.glass_type,
            'glass_color_id': line.glass_color_id,
            'thickness': line.thickness,
            'extra_id': line.extra_id,
            'unit': line.unit,
            'total_unit': line.total_unit,
            'fly_screen': line.fly_screen,
            'unit_fly': line.unit_fly,
            'total_fly': line.total_fly,
            'design': line.design,
        }

    # This function using to get quotation template from quotation.template model
    @api.onchange('quot_template')
    def onchange_quot_template(self):
        order_lines = [(5, 0, 0)]
        for line in self.quot_template.quotaion_id:
            data = self._compute_line_data_for_template_change(line)
            if line.system_id:
                data.update({
                    'code': line.code,
                    'unit_loc': line.unit_loc,
                    'system_id': line.system_id,
                    'profile_type_ids': line.profile_type_ids,
                    'profile_color_ids': line.profile_color_ids,
                    'dim_w': line.dim_w,
                    'dim_h': line.dim_h,
                    'area': line.area,
                    'qty': line.qty,
                    'total': line.total,
                    'extra': line.extra,
                    # 'vertical': line.vertical,
                    # 'horizontal': line.horizontal,
                    'glass_type': line.glass_type,
                    'glass_color_id': line.glass_color_id,
                    'thickness': line.thickness,
                    'extra_id': line.extra_id,
                    'unit': line.unit,
                    'total_unit': line.total_unit,
                    'fly_screen': line.fly_screen,
                    'unit_fly': line.unit_fly,
                    'total_fly': line.total_fly,
                    'design': line.design,
                })
            order_lines.append((0, 0, data))

        self.quotaion_id = order_lines

    @api.depends('quotaion_id')
    def get_total_all(self):
        for rec in self:
            total = 0.0
            for tot in rec.quotaion_id:
                total += tot.total_fly
            rec.total_all = total




class QuotaionLine(models.Model):
    _name = 'omsi.quotaion.line'

    quotaion_ids = fields.Many2one('omsi.quotaion')
    seq = fields.Integer()
    code = fields.Integer('Code')
    unit_loc = fields.Char('Unit Location')
    system_id = fields.Many2one('system.template')
    profile_type_ids = fields.Many2one('profile.type', domain="[('system_ids','=',system_id)]")
    profile_color_ids = fields.Many2one('paints', domain="[('system_id','=',system_id)]")
    dim_w = fields.Float('W', related='system_id.w')
    dim_h = fields.Float('H', related='system_id.h')
    area = fields.Float('Area', compute='get_area')
    qty = fields.Integer('Qty', default=1)
    total = fields.Float('Total Area', compute='get_total')
    extra = fields.Float('Extra')
    vertical = fields.Float('Mullion Vertical')
    horizontal = fields.Float('Mullion Horizontal')
    glass_type = fields.Selection([
        ('single_glass', ' مفرد '),
        ('double_glass', ' مزدوج ')],
        string='Glass Type', default="single_glass")
    glass_color_id = fields.Many2one('glass.color', string='Glass Color', domain="[('glass_type','=',glass_type)]")
    thickness = fields.Selection([
        ('six_mm', ' 6 مم '),
        ('twelve_mm', ' 12 مم '),
        ('eighteen_mm', ' 18 مم '),
        ('laminitude', ' ﻻمينيتد ')
    ], string='Thickness', default="six_mm")
    extra_id = fields.Many2one('glass', domain="[('glass_type','=',glass_type)]")

    unit = fields.Float('Unit', compute='get_unit', digits=(16,0))
    total_unit = fields.Float('Total', compute='get_unit_total', digits=(16,0))
    fly_screen = fields.Selection([
        ('without', ' بدون '),
        ('tractor', ' جرار '),
        ('only_you_see', ' تراك فقط ')
    ],
        string='Fly Screen', default="")
    unit_fly = fields.Float('Unit', compute='get_unit_fly_screen', digits=(16,0))
    total_fly = fields.Float('Total', compute='get_unit_fly_total', digits=(16,0))

    design = fields.Binary('Design')

    @api.depends('dim_w', 'dim_h')
    def get_area(self):
        for rec in self:
            rec.area = rec.dim_w * rec.dim_h

    @api.depends('area', 'qty')
    def get_total(self):
        for rec in self:
            rec.total = rec.area * int(rec.qty)

    @api.depends('system_id', 'profile_color_ids', 'glass_color_id')
    def get_unit(self):
        for rec in self:
            rec.unit = (rec.system_id.accessories_id.total) + (rec.system_id.sector_id.total) + (
                rec.profile_color_ids.total) + (rec.glass_color_id.total) + (rec.extra_id.total)

    @api.depends('system_id', 'profile_color_ids', 'glass_color_id', 'fly_screen')
    def get_unit_fly_screen(self):
        for rec in self:
            if rec.fly_screen == 'tractor':
                rec.unit_fly = (rec.system_id.accessories_id.total) + (
                    rec.system_id.sector_id.total) + (rec.profile_color_ids.total) + (rec.glass_color_id.total) + (
                                   rec.extra_id.total) + (rec.system_id.fly_id.total) + (
                                   rec.system_id.fly_accessor_id.total)
            else:
                rec.unit_fly = (rec.system_id.accessories_id.total) + (rec.system_id.sector_id.total) + (
                    rec.profile_color_ids.total) + (rec.glass_color_id.total) + (rec.extra_id.total)

    @api.depends('unit', 'qty')
    def get_unit_total(self):
        for rec in self:
            rec.total_unit = rec.unit * rec.qty

    @api.depends('unit_fly', 'qty')
    def get_unit_fly_total(self):
        for rec in self:
            rec.total_fly = rec.unit_fly * rec.qty

