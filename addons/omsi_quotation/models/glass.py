
from odoo import models,fields,api,_
from odoo.exceptions import ValidationError

class Glass(models.Model):
    _name = 'glass'
    _rec_name = 'desc'

    name = fields.Many2one('system.template')
    profile_type_ids = fields.Many2one('profile.type', domain="[('system_ids','=', name)]")
    margin = fields.Float('Margin')
    glass_type = fields.Selection([
        ('single_glass', ' مفرد '),
        ('double_glass', ' مزدوج ')],
        string='Glass Type', default="single_glass")
    w = fields.Float('W', related='name.w', readonly=False)
    h = fields.Float('H', related='name.h', readonly=False)
    qty = fields.Float('Qty', compute='get_qty')
    desc = fields.Char('Description')
    price_id = fields.Many2one('glass.price')
    purchase_price = fields.Float('Purchase Price' ,digits=(16, 3))
    sale_price = fields.Float('Sale Price', compute='get_sale', digits=(16, 3))
    total = fields.Float('Total', compute='get_total', digits=(16, 3))
    glass_id = fields.Selection([
        ('security', ' سكيوريتى '),
        ('lamenety', ' ﻻمينيتيد '),
        ('security_lamenety', 'سكيوريتى و ﻻمينيتيد '),
        ('gorgian', ' جورجيان '),
        ('securit_gorgian', ' سكيوريت وجورجيان '),
        ('gorgian_lamenety', ' جورجيان و ﻻمبينيتد '),
        ('security_lamenety_gorgian', ' سكيوريت وجورجيان و ﻻمبينيتد '),
    ],string='Glass Single', default="security")
    color_id = fields.Many2one('glass.color', store=True)


    @api.depends('w', 'h')
    def get_qty(self):
        for rec in self:
            rec.qty = rec.w * rec.h

    @api.depends('purchase_price', 'margin')
    def get_sale(self):
        for rec in self:
            rec.sale_price = rec.purchase_price * rec.margin

    @api.depends('sale_price', 'qty','glass_id','name','price_id')
    def get_total(self):
        for rec in self:
            if rec.glass_id:
                if rec.glass_id in ('security'):
                    if rec.glass_type in ('single_glass'):
                        rec.total = (rec.sale_price * rec.qty)
                    else:
                        rec.total = (rec.sale_price * rec.qty * 2.0)
                elif rec.glass_id in ('lamenety'):
                    rec.total = (rec.sale_price * rec.qty) + (rec.name.price_id.clear_glass_6 * rec.margin * rec.qty)
                elif rec.glass_id in ('security_lamenety'):
                    if rec.glass_type in ('single_glass'):
                        rec.total = (rec.sale_price * rec.qty) + (
                                    rec.name.price_id.clear_glass_6 * rec.margin * rec.qty) + (
                                                (rec.name.price_id.security * 2) * rec.margin * rec.qty)
                    else:
                        rec.total = (rec.sale_price * rec.qty) + (
                                    rec.name.price_id.clear_glass_6 * rec.margin * rec.qty) + (
                                                (rec.name.price_id.security * 3) * rec.margin * rec.qty)
                elif rec.glass_id in ('gorgian'):
                    rec.total = rec.sale_price * ((rec.w * 3.0) + (rec.h * 3.0))
                elif rec.glass_id in ('securit_gorgian'):
                    rec.total = (((rec.name.price_id.security * 2.0)) * rec.margin) + ((((rec.w * 3.0) + (rec.h * 3.0)) * (rec.name.price_id.gorgian)) * rec.margin)

                elif rec.glass_id in ('gorgian_lamenety'):
                    rec.total = (((rec.name.price_id.security * 3.0)) * rec.margin) + (((rec.name.price_id.lamenety)) * rec.margin) + (rec.name.price_id.clear_glass_6 * rec.margin * rec.qty) + (((rec.name.price_id.gorgian * 6.0)) * rec.margin)
                elif rec.glass_id in ('security_lamenety_gorgian'):
                    rec.total = (((rec.name.price_id.security * 3.0)) * rec.margin) + (((rec.name.price_id.security_lamenety)) * rec.margin) + (rec.name.price_id.clear_glass_6 * rec.margin * rec.qty)  + (((rec.name.price_id.gorgian * 6.0)) * rec.margin)
                else:
                    rec.total = 0
            else:
                raise ValidationError(_("You Must Enter Value In Glass Field ."))