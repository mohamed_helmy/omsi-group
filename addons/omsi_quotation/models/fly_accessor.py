from odoo import models, fields, api


class FlyAccessor(models.Model):
    _name = 'fly.accessor'
    _rec_name = 'accessor_name'

    accessor_name = fields.Char('Fly Screen Accessor')
    name = fields.Many2one('system.template')
    profile_type_ids = fields.Many2one('profile.type', domain="[('system_ids','=', name)]")
    margin = fields.Float('Margin', default=1.90)
    quot_template = fields.Many2one('quotation.template', string='Quotation Template')
    type = fields.Selection([
        ('two_dalfa', ' جرار 2 ضلفه '),
        ('tree_dalfa', ' جرار 3 ضلفة '),
        ('four_dalfa', ' جرار 4 ضلفة ')],
        string='Design', required=True, default="two_dalfa")
    profile_types = fields.Selection([
        ('jumbo', ' جامبو '),
        ('tango', ' تانجو '),
        ('ps_9600', ' PS 9600 ')
    ],
        string='Profile', required=True, default="jumbo")
    kilo_rate = fields.Float('Kilo Rate', default=58.71)
    w = fields.Float('W', related='name.w', readonly=False)
    h = fields.Float('H', related='name.h', readonly=False)
    total = fields.Float('Total', compute='get_total')

    ########################################################################################################################

    ###################################
    ###########    Jumbo   ############
    ###################################

    # line  4  ==>      Epdm-181

    desc4 = fields.Char('Description', default='Epdm-181')
    desc4_qty = fields.Float('Qty', compute='get_qty4', readonly=False)
    desc4_purchase = fields.Float('Purchase', default=2.0)
    desc4_sale = fields.Float('Sale', compute='get_desc4_sale')
    desc4_total = fields.Float('Total', compute='get_desc4_total')

    @api.depends('type')
    def get_qty4(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc4_qty = (rec.w * 1.0) + (rec.h * 2.0)
            else:
                rec.desc4_qty = (rec.w * 1.0) + (rec.h * 4.0)

    @api.depends('margin', 'desc4_purchase')
    def get_desc4_sale(self):
        for rec in self:
            rec.desc4_sale = rec.margin * rec.desc4_purchase

    @api.depends('desc4_qty', 'desc4_sale')
    def get_desc4_total(self):
        for rec in self:
            rec.desc4_total = rec.desc4_qty * rec.desc4_sale

    ########################################################################################################################
    # line  5  ==>     Forsh- 5*10

    desc5 = fields.Char('Description', default='Forsh- 5*10')
    desc5_qty = fields.Float('Qty', compute='get_qty5', readonly=False)
    desc5_purchase = fields.Float('Purchase', default=0.75)
    desc5_sale = fields.Float('Sale', compute='get_desc5_sale')
    desc5_total = fields.Float('Total', compute='get_desc5_total')

    @api.depends('type')
    def get_qty5(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc5_qty = (rec.w * 4.0) + (rec.h * 6.0)
            elif rec.type in ('four_dalfa'):
                rec.desc5_qty = (rec.w * 4.0) + (rec.h * 8.0)
            else:
                rec.desc5_qty = (rec.w * 2.0) + (rec.h * 4.0)

    @api.depends('margin', 'desc5_purchase')
    def get_desc5_sale(self):
        for rec in self:
            rec.desc5_sale = rec.margin * rec.desc5_purchase

    @api.depends('desc5_qty', 'desc5_sale')
    def get_desc5_total(self):
        for rec in self:
            rec.desc5_total = rec.desc5_qty * rec.desc5_sale

    ########################################################################################################################
    # line  6  ==>     S.C - 3601

    desc6 = fields.Char('Description', default='S.C - 3601')
    desc6_qty = fields.Float('Qty', compute='get_qty6', readonly=False)
    desc6_purchase = fields.Float('Purchase', default=5.0)
    desc6_sale = fields.Float('Sale', compute='get_desc6_sale')
    desc6_total = fields.Float('Total', compute='get_desc6_total')

    @api.depends('type')
    def get_qty6(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc6_qty = 2.0
            else:
                rec.desc6_qty = 4.0

    @api.depends('margin', 'desc6_purchase')
    def get_desc6_sale(self):
        for rec in self:
            rec.desc6_sale = rec.margin * rec.desc6_purchase

    @api.depends('desc6_qty', 'desc6_sale')
    def get_desc6_total(self):
        for rec in self:
            rec.desc6_total = rec.desc6_qty * rec.desc6_sale

########################################################################################################################
    # line  7  ==>     سلك فيبر

    desc7 = fields.Char('Description', default=' سلك فيبر ')
    desc7_qty = fields.Float('Qty', compute='get_qty7', readonly=False)
    desc7_purchase = fields.Float('Purchase', default=15.0)
    desc7_sale = fields.Float('Sale', compute='get_desc7_sale')
    desc7_total = fields.Float('Total', compute='get_desc7_total')

    @api.depends('type')
    def get_qty7(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc7_qty = ((rec.w * rec.h) * 65) / 100
            else:
                rec.desc7_qty = (rec.w * rec.h) / 2.0

    @api.depends('margin', 'desc7_purchase')
    def get_desc7_sale(self):
        for rec in self:
            rec.desc7_sale = rec.margin * rec.desc7_purchase

    @api.depends('desc7_qty', 'desc7_sale')
    def get_desc7_total(self):
        for rec in self:
            rec.desc7_total = rec.desc7_qty * rec.desc7_sale

    ########################################################################################################################

    ###################################
    ###########    Tango   ############
    ###################################

    # line 8  ==>   Epdm-152
    desc8 = fields.Char('Description', default='Epdm-152')
    desc8_qty = fields.Float('Qty', compute='get_qty8')
    desc8_purchase = fields.Float('Purchase', default=2)
    desc8_sale = fields.Float('Sale', compute='get_desc8_sale')
    desc8_total = fields.Float('Total', compute='get_desc8_total')

    @api.depends('type')
    def get_qty8(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc8_qty = (rec.w * 2.0 * 0.65) + (rec.h * 2.0)
            elif rec.type in ('four_dalfa'):
                rec.desc8_qty = (rec.w * 1.0) + (rec.h * 4.0)
            else:
                rec.desc8_qty = (rec.w * 1.0) + (rec.h * 2.0)

    @api.depends('margin', 'desc8_purchase')
    def get_desc8_sale(self):
        for rec in self:
            rec.desc8_sale = rec.margin * rec.desc8_purchase

    @api.depends('desc8_qty', 'desc8_sale')
    def get_desc8_total(self):
        for rec in self:
            rec.desc8_total = rec.desc8_qty * rec.desc8_sale

    ########################################################################################################################

    # line 9  ==>   Epdm-181
    desc9 = fields.Char('Description', default='Epdm-181')
    desc9_qty = fields.Float('Qty', compute='get_qty9')
    desc9_purchase = fields.Float('Purchase', default=2)
    desc9_sale = fields.Float('Sale', compute='get_desc9_sale')
    desc9_total = fields.Float('Total', compute='get_desc9_total')

    @api.depends('type')
    def get_qty9(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc9_qty = (rec.w * 2.0 * 0.65) + (rec.h * 2.0)
            elif rec.type in ('four_dalfa'):
                rec.desc9_qty = (rec.w * 1.0) + (rec.h * 4.0)
            else:
                rec.desc9_qty = (rec.w * 1.0) + (rec.h * 2.0)

    @api.depends('margin', 'desc9_purchase')
    def get_desc9_sale(self):
        for rec in self:
            rec.desc9_sale = rec.margin * rec.desc9_purchase

    @api.depends('desc9_qty', 'desc9_sale')
    def get_desc9_total(self):
        for rec in self:
            rec.desc9_total = rec.desc9_qty * rec.desc9_sale

    ########################################################################################################################

    # line 10  ==>    Forsh- 5*10
    desc10 = fields.Char('Description', default='Forsh- 5*10')
    desc10_qty = fields.Float('Qty', compute='get_qty10')
    desc10_purchase = fields.Float('Purchase', default=0.75)
    desc10_sale = fields.Float('Sale', compute='get_desc10_sale')
    desc10_total = fields.Float('Total', compute='get_desc10_total')

    @api.depends('type')
    def get_qty10(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc10_qty = (rec.w * 4.0 * 0.65) + (rec.h * 4.0)
            elif rec.type in ('four_dalfa'):
                rec.desc10_qty = (rec.w * 4.0) + (rec.h * 8.0)
            else:
                rec.desc10_qty = (rec.w * 2.0) + (rec.h * 4.0)

    @api.depends('margin', 'desc10_purchase')
    def get_desc10_sale(self):
        for rec in self:
            rec.desc10_sale = rec.margin * rec.desc10_purchase

    @api.depends('desc10_qty', 'desc10_sale')
    def get_desc10_total(self):
        for rec in self:
            rec.desc10_total = rec.desc10_qty * rec.desc10_sale

    ########################################################################################################################

    # line 11  ==>     S.C - 3601
    desc11 = fields.Char('Description', default='S.C - 3601')
    desc11_qty = fields.Float('Qty', compute='get_qty11')
    desc11_purchase = fields.Float('Purchase', default=5.0)
    desc11_sale = fields.Float('Sale', compute='get_desc11_sale')
    desc11_total = fields.Float('Total', compute='get_desc11_total')

    @api.depends('type')
    def get_qty11(self):
        for rec in self:
            if rec.type in ('four_dalfa'):
                rec.desc11_qty = 4.0
            else:
                rec.desc11_qty = 2.0

    @api.depends('margin', 'desc11_purchase')
    def get_desc11_sale(self):
        for rec in self:
            rec.desc11_sale = rec.margin * rec.desc11_purchase

    @api.depends('desc11_qty', 'desc11_sale')
    def get_desc11_total(self):
        for rec in self:
            rec.desc11_total = rec.desc11_qty * rec.desc11_sale

    ########################################################################################################################

    # line 12  ==>     سلك فيبر
    desc12 = fields.Char('Description', default='سلك فيبر')
    desc12_qty = fields.Float('Qty', compute='get_qty12')
    desc12_purchase = fields.Float('Purchase', default=15.0)
    desc12_sale = fields.Float('Sale', compute='get_desc12_sale')
    desc12_total = fields.Float('Total', compute='get_desc12_total')

    @api.depends('type')
    def get_qty12(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc12_qty = 0.65
            else:
                rec.desc12_qty = 0.5 * (rec.w * rec.h)

    @api.depends('margin', 'desc12_purchase')
    def get_desc12_sale(self):
        for rec in self:
            rec.desc12_sale = rec.margin * rec.desc12_purchase

    @api.depends('desc12_qty', 'desc12_sale')
    def get_desc12_total(self):
        for rec in self:
            rec.desc12_total = rec.desc12_qty * rec.desc12_sale

########################################################################################################################

    #####################################
    ###########    PS 9600   ############
    #####################################

########################################################################################################################

    # line 13  ==>    طقم عجل وكورنر سلك
    desc13 = fields.Char('Description', default=' طقم عجل وكورنر سلك ')
    desc13_qty = fields.Float('Qty', compute='get_qty13')
    desc13_purchase = fields.Float('Purchase', default=20.0)
    desc13_sale = fields.Float('Sale', compute='get_desc13_sale')
    desc13_total = fields.Float('Total', compute='get_desc13_total')

    @api.depends('type')
    def get_qty13(self):
        for rec in self:
            if rec.type in ('four_dalfa'):
                rec.desc13_qty = 2.0
            else:
                rec.desc13_qty = 1.0

    @api.depends('margin', 'desc13_purchase')
    def get_desc13_sale(self):
        for rec in self:
            rec.desc13_sale = rec.margin * rec.desc13_purchase

    @api.depends('desc13_qty', 'desc13_sale')
    def get_desc13_total(self):
        for rec in self:
            rec.desc13_total = rec.desc13_qty * rec.desc13_sale


########################################################################################################################

    # line 14  ==>    Forsh- 5*10
    desc14 = fields.Char('Description', default='Forsh- 5*10')
    desc14_qty = fields.Float('Qty', compute='get_qty14')
    desc14_purchase = fields.Float('Purchase', default=0.75)
    desc14_sale = fields.Float('Sale', compute='get_desc14_sale')
    desc14_total = fields.Float('Total', compute='get_desc14_total')

    @api.depends('type')
    def get_qty14(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc14_qty = (rec.w * 1.0) + (rec.h * 2.0)
            else:
                rec.desc14_qty = (rec.w * 1.0) + (rec.h * 4.0)

    @api.depends('margin', 'desc14_purchase')
    def get_desc14_sale(self):
        for rec in self:
            rec.desc14_sale = rec.margin * rec.desc14_purchase

    @api.depends('desc14_qty', 'desc14_sale')
    def get_desc14_total(self):
        for rec in self:
            rec.desc14_total = rec.desc14_qty * rec.desc14_sale

########################################################################################################################

    # line 15  ==>   كاوتش لضلفه السلك
    desc15 = fields.Char('Description', default=' كاوتش لضلفه السلك ')
    desc15_qty = fields.Float('Qty', compute='get_qty15')
    desc15_purchase = fields.Float('Purchase', default=1.0)
    desc15_sale = fields.Float('Sale', compute='get_desc15_sale')
    desc15_total = fields.Float('Total', compute='get_desc15_total')

    @api.depends('type')
    def get_qty15(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc15_qty = (rec.w * 1.0) + (rec.h * 2.0)
            else:
                rec.desc15_qty = (rec.w * 1.0) + (rec.h * 4.0)

    @api.depends('margin', 'desc15_purchase')
    def get_desc15_sale(self):
        for rec in self:
            rec.desc15_sale = rec.margin * rec.desc15_purchase

    @api.depends('desc15_qty', 'desc15_sale')
    def get_desc15_total(self):
        for rec in self:
            rec.desc15_total = rec.desc15_qty * rec.desc15_sale

########################################################################################################################
    # line  16  ==>     سلك فيبر

    desc16 = fields.Char('Description', default=' سلك فيبر ')
    desc16_qty = fields.Float('Qty', compute='get_qty16', readonly=False)
    desc16_purchase = fields.Float('Purchase', default=15.0)
    desc16_sale = fields.Float('Sale', compute='get_desc16_sale')
    desc16_total = fields.Float('Total', compute='get_desc16_total')

    @api.depends('type')
    def get_qty16(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc16_qty = ((rec.w * rec.h) * 65) / 100
            else:
                rec.desc16_qty = (rec.w * rec.h) / 2.0

    @api.depends('margin', 'desc16_purchase')
    def get_desc16_sale(self):
        for rec in self:
            rec.desc16_sale = rec.margin * rec.desc16_purchase

    @api.depends('desc16_qty', 'desc16_sale')
    def get_desc16_total(self):
        for rec in self:
            rec.desc16_total = rec.desc16_qty * rec.desc16_sale

########################################################################################################################

    @api.depends('profile_types', 'desc4_total', 'desc5_total', 'desc6_total', 'desc7_total', 'desc8_total',
                 'desc9_total', 'desc10_total', 'desc11_total', 'desc12_total', 'desc13_total', 'desc14_total',
                 'desc15_total', 'desc16_total')
    def get_total(self):
        for rec in self:
            if rec.profile_types == 'jumbo':
                rec.total = rec.desc4_total + rec.desc5_total + rec.desc6_total + rec.desc7_total
            elif rec.profile_types == 'tango':
                rec.total = rec.desc8_total + rec.desc9_total + rec.desc10_total + rec.desc11_total + rec.desc12_total
            elif rec.profile_types == 'ps_9600':
                rec.total = rec.desc13_total + rec.desc14_total + rec.desc15_total + rec.desc16_total
            else:
                rec.total = 0

########################################################################################################################
