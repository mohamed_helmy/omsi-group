from odoo import models, fields, api


class Accessories(models.Model):
    _name = 'accessories'
    _rec_name = 'access_name'

    access_name = fields.Char('Accessories Name')
    name = fields.Many2one('system.template')
    profile_type_ids = fields.Many2one('profile.type', domain="[('system_ids','=', name)]")
    margin = fields.Float('Margin', default=1.90)
    quot_template = fields.Many2one('quotation.template', string='Quotation Template')
    type = fields.Selection([
        ('two_dalfa', ' جرار 2 ضلفه '),
        ('tree_dalfa', ' جرار 3 ضلفة '),
        ('four_dalfa', ' جرار 4 ضلفة ')],
        string='Design', required=True, default="two_dalfa", )
    profile_types = fields.Selection([
        ('jumbo', ' جامبو '),
        ('tango', ' تانجو '),
        ('ps_9600', ' PS 9600 ')
    ],
        string='Profile', required=True, default="jumbo")
    w = fields.Float('W', related='name.w', readonly=False)
    h = fields.Float('H', related='name.h', readonly=False)
    total = fields.Float('Total', compute='get_total')

########################################################################################################################

    ###################################
    ###########    Jumbo   ############
    ###################################

########################################################################################################################

    # line  1  ==>  COR-301
    desc1 = fields.Char('Description', default='COR-301')
    desc1_qty = fields.Float('Qty', default=4.0)
    desc1_purchase = fields.Float('Purchase', default=5.15)
    desc1_sale = fields.Float('Sale', compute='get_desc1_sale')
    desc1_total = fields.Float('Total', compute='get_desc1_total')

    @api.depends('margin', 'desc1_purchase')
    def get_desc1_sale(self):
        for rec in self:
            rec.desc1_sale = rec.margin * rec.desc1_purchase

    @api.depends('desc1_qty', 'desc1_sale')
    def get_desc1_total(self):
        for rec in self:
            rec.desc1_total = rec.desc1_qty * rec.desc1_sale

    ########################################################################################################################

    # line 2  ==>  COR-320
    desc2 = fields.Char('Description', default='COR-320')
    desc2_qty = fields.Float('Qty', compute='get_qty2', readonly=False)
    desc2_purchase = fields.Float('Purchase', default=3.65)
    desc2_sale = fields.Float('Sale', compute='get_desc2_sale')
    desc2_total = fields.Float('Total', compute='get_desc2_total')

    @api.depends('type')
    def get_qty2(self):
        for rec in self:
            if rec.type in ('four_dalfa'):
                rec.desc2_qty = 16.0
            else:
                rec.desc2_qty = 8.0

    @api.depends('margin', 'desc2_purchase')
    def get_desc2_sale(self):
        for rec in self:
            rec.desc2_sale = rec.margin * rec.desc2_purchase

    @api.depends('desc2_qty', 'desc1_sale')
    def get_desc2_total(self):
        for rec in self:
            rec.desc2_total = rec.desc2_qty * rec.desc2_sale

    ########################################################################################################################

    # line 3  ==>  Plast-550
    desc3 = fields.Char('Description', default='Plast-550')
    desc3_qty = fields.Float('Qty', compute='get_qty3')
    desc3_purchase = fields.Float('Purchase', default=0.07)
    desc3_sale = fields.Float('Sale', compute='get_desc3_sale')
    desc3_total = fields.Float('Total', compute='get_desc3_total')

    @api.depends('w', 'h')
    def get_qty3(self):
        # for all design
        for rec in self:
            rec.desc3_qty = (((rec.w * 2.0) + (rec.h * 2.0)) * 100) / 30

    @api.depends('margin', 'desc3_purchase')
    def get_desc3_sale(self):
        for rec in self:
            rec.desc3_sale = rec.margin * rec.desc3_purchase

    @api.depends('desc3_qty', 'desc3_sale')
    def get_desc3_total(self):
        for rec in self:
            rec.desc3_total = rec.desc3_qty * rec.desc3_sale

    ########################################################################################################################

    # line 4  ==>  Plast-570
    desc4 = fields.Char('Description', default='Plast-570')
    desc4_qty = fields.Float('Qty', compute='get_qty4')
    desc4_purchase = fields.Float('Purchase', default=1.0)
    desc4_sale = fields.Float('Sale', compute='get_desc4_sale')
    desc4_total = fields.Float('Total', compute='get_desc4_total')

    @api.depends('type')
    def get_qty4(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc4_qty = 24.0
            elif rec.type in ('four_dalfa'):
                rec.desc4_qty = 32.0
            else:
                rec.desc4_qty = 16.0

    @api.depends('margin', 'desc4_purchase')
    def get_desc4_sale(self):
        for rec in self:
            rec.desc4_sale = rec.margin * rec.desc4_purchase

    @api.depends('desc4_qty', 'desc4_sale')
    def get_desc4_total(self):
        for rec in self:
            rec.desc4_total = rec.desc4_qty * rec.desc4_sale

    ########################################################################################################################

    # line 5   ==>  ALU-572
    desc5 = fields.Char('Description', default='ALU-572')
    desc5_qty = fields.Float('Qty', default=4.0)
    desc5_purchase = fields.Float('Purchase', default=1.0)
    desc5_sale = fields.Float('Sale', compute='get_desc5_sale')
    desc5_total = fields.Float('Total', compute='get_desc5_total')

    @api.depends('margin', 'desc5_purchase')
    def get_desc5_sale(self):
        for rec in self:
            rec.desc5_sale = rec.margin * rec.desc5_purchase

    @api.depends('desc5_qty', 'desc5_sale')
    def get_desc5_total(self):
        for rec in self:
            rec.desc5_total = rec.desc5_qty * rec.desc5_sale

    ########################################################################################################################

    # line 5   ==>   ALU-574
    desc6 = fields.Char('Description', default='ALU-574')
    desc6_qty = fields.Float('Qty', default=4.0)
    desc6_purchase = fields.Float('Purchase', default=1.0)
    desc6_sale = fields.Float('Sale', compute='get_desc6_sale')
    desc6_total = fields.Float('Total', compute='get_desc6_total')

    @api.depends('margin', 'desc6_purchase')
    def get_desc6_sale(self):
        for rec in self:
            rec.desc6_sale = rec.margin * rec.desc6_purchase

    @api.depends('desc6_qty', 'desc6_sale')
    def get_desc6_total(self):
        for rec in self:
            rec.desc6_total = rec.desc6_qty * rec.desc6_sale

    ########################################################################################################################

    # line 7  ==>   Zama-3004
    desc7 = fields.Char('Description', default='Zama-3004')
    desc7_qty = fields.Float('Qty', compute='get_qty7')
    desc7_purchase = fields.Float('Purchase', default=3.5)
    desc7_sale = fields.Float('Sale', compute='get_desc7_sale')
    desc7_total = fields.Float('Total', compute='get_desc7_total')

    @api.depends('type')
    def get_qty7(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc7_qty = 3.0
            else:
                rec.desc7_qty = 2.0

    @api.depends('margin', 'desc7_purchase')
    def get_desc7_sale(self):
        for rec in self:
            rec.desc7_sale = rec.margin * rec.desc7_purchase

    @api.depends('desc7_qty', 'desc7_sale')
    def get_desc7_total(self):
        for rec in self:
            rec.desc7_total = rec.desc7_qty * rec.desc7_sale

    ########################################################################################################################

    # line 8  ==>    IH-247
    desc8 = fields.Char('Description', default='IH-247')
    desc8_qty = fields.Float('Qty', compute='get_qty8')
    desc8_purchase = fields.Float('Purchase', default=40.0)
    desc8_sale = fields.Float('Sale', compute='get_desc8_sale')
    desc8_total = fields.Float('Total', compute='get_desc8_total')

    @api.depends('type')
    def get_qty8(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc8_qty = 3.0
            else:
                rec.desc8_qty = 2.0

    @api.depends('margin', 'desc8_purchase')
    def get_desc8_sale(self):
        for rec in self:
            rec.desc8_sale = rec.margin * rec.desc8_purchase

    @api.depends('desc8_qty', 'desc8_sale')
    def get_desc8_total(self):
        for rec in self:
            rec.desc8_total = rec.desc8_qty * rec.desc8_sale

    ########################################################################################################################

    # line  9  ==>   3050
    desc9 = fields.Char('Description', default='3050')
    desc9_qty = fields.Float('Qty', default=1.0)
    desc9_purchase = fields.Float('Purchase', default=20.0)
    desc9_sale = fields.Float('Sale', compute='get_desc9_sale')
    desc9_total = fields.Float('Total', compute='get_desc9_total')

    @api.depends('margin', 'desc9_purchase')
    def get_desc9_sale(self):
        for rec in self:
            rec.desc9_sale = rec.margin * rec.desc9_purchase

    @api.depends('desc9_qty', 'desc9_sale')
    def get_desc9_total(self):
        for rec in self:
            rec.desc9_total = rec.desc9_qty * rec.desc9_sale

    ########################################################################################################################

    # line 10  ==>   Roll-3295-N
    desc10 = fields.Char('Description', default='Roll-3295-N')
    desc10_qty = fields.Float('Qty', compute='get_qty10')
    desc10_purchase = fields.Float('Purchase', default=20.0)
    desc10_sale = fields.Float('Sale', compute='get_desc10_sale')
    desc10_total = fields.Float('Total', compute='get_desc10_total')

    @api.depends('type')
    def get_qty10(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc10_qty = 6.0
            elif rec.type in ('four_dalfa'):
                rec.desc10_qty = 8.0
            else:
                rec.desc10_qty = 4.0

    @api.depends('margin', 'desc10_purchase')
    def get_desc10_sale(self):
        for rec in self:
            rec.desc10_sale = rec.margin * rec.desc10_purchase

    @api.depends('desc10_qty', 'desc10_sale')
    def get_desc10_total(self):
        for rec in self:
            rec.desc10_total = rec.desc10_qty * rec.desc10_sale

    ########################################################################################################################

    # line 11  ==>    Antidust-3395
    desc11 = fields.Char('Description', default='Antidust-3395')
    desc11_qty = fields.Float('Qty', compute='get_qty11')
    desc11_purchase = fields.Float('Purchase', default=5.0)
    desc11_sale = fields.Float('Sale', compute='get_desc11_sale')
    desc11_total = fields.Float('Total', compute='get_desc11_total')

    @api.depends('type')
    def get_qty11(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc11_qty = 2.0
            else:
                rec.desc11_qty = 4.0

    @api.depends('margin', 'desc11_purchase')
    def get_desc11_sale(self):
        for rec in self:
            rec.desc11_sale = rec.margin * rec.desc11_purchase

    @api.depends('desc11_qty', 'desc11_sale')
    def get_desc11_total(self):
        for rec in self:
            rec.desc11_total = rec.desc11_qty * rec.desc11_sale

    ########################################################################################################################

    # line 12  ==>     Plast-3331
    desc12 = fields.Char('Description', default='Plast-3331')
    desc12_qty = fields.Float('Qty', compute='get_qty12')
    desc12_purchase = fields.Float('Purchase', default=1.25)
    desc12_sale = fields.Float('Sale', compute='get_desc12_sale')
    desc12_total = fields.Float('Total', compute='get_desc12_total')

    @api.depends('type')
    def get_qty12(self):
        for rec in self:
            if rec.type in ('four_dalfa'):
                rec.desc12_qty = 6.0
            else:
                rec.desc12_qty = 4.0

    @api.depends('margin', 'desc12_purchase')
    def get_desc12_sale(self):
        for rec in self:
            rec.desc12_sale = rec.margin * rec.desc12_purchase

    @api.depends('desc12_qty', 'desc12_sale')
    def get_desc12_total(self):
        for rec in self:
            rec.desc12_total = rec.desc12_qty * rec.desc12_sale

    ########################################################################################################################

    # line 13  ==>      Plast-3495
    desc13 = fields.Char('Description', default='Plast-3495')
    desc13_qty = fields.Float('Qty', compute='get_qty13')
    desc13_purchase = fields.Float('Purchase', default=1.25)
    desc13_sale = fields.Float('Sale', compute='get_desc13_sale')
    desc13_total = fields.Float('Total', compute='get_desc13_total')

    @api.depends('type')
    def get_qty13(self):
        for rec in self:
            if rec.type in ('four_dalfa'):
                rec.desc13_qty = 8.0
            else:
                rec.desc13_qty = 4.0

    @api.depends('margin', 'desc13_purchase')
    def get_desc13_sale(self):
        for rec in self:
            rec.desc13_sale = rec.margin * rec.desc13_purchase

    @api.depends('desc13_qty', 'desc13_sale')
    def get_desc13_total(self):
        for rec in self:
            rec.desc13_total = rec.desc13_qty * rec.desc13_sale

    ########################################################################################################################

    # line 14  ==>       Plast-3496
    desc14 = fields.Char('Description', default='Plast-3496')
    desc14_qty = fields.Float('Qty', compute='get_qty14')
    desc14_purchase = fields.Float('Purchase', default=5.0)
    desc14_sale = fields.Float('Sale', compute='get_desc14_sale')
    desc14_total = fields.Float('Total', compute='get_desc14_total')

    @api.depends('type')
    def get_qty14(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc14_qty = 2.0
            else:
                rec.desc14_qty = 4.0

    @api.depends('margin', 'desc14_purchase')
    def get_desc14_sale(self):
        for rec in self:
            rec.desc14_sale = rec.margin * rec.desc14_purchase

    @api.depends('desc14_qty', 'desc14_sale')
    def get_desc14_total(self):
        for rec in self:
            rec.desc14_total = rec.desc14_qty * rec.desc14_sale

    ########################################################################################################################

    # line 15  ==>       Plast-3497
    desc15 = fields.Char('Description', default='Plast-3497')
    desc15_qty = fields.Float('Qty', compute='get_qty15')
    desc15_purchase = fields.Float('Purchase', default=2.50)
    desc15_sale = fields.Float('Sale', compute='get_desc15_sale')
    desc15_total = fields.Float('Total', compute='get_desc15_total')

    @api.depends('type')
    def get_qty15(self):
        for rec in self:
            if rec.type in ('four_dalfa'):
                rec.desc15_qty = 8.0
            else:
                rec.desc15_qty = 4.0

    @api.depends('margin', 'desc15_purchase')
    def get_desc15_sale(self):
        for rec in self:
            rec.desc15_sale = rec.margin * rec.desc15_purchase

    @api.depends('desc15_qty', 'desc15_sale')
    def get_desc15_total(self):
        for rec in self:
            rec.desc15_total = rec.desc15_qty * rec.desc15_sale

    ########################################################################################################################

    # line 16  ==>        Plast-3510
    desc16 = fields.Char('Description', default='Plast-3510')
    desc16_qty = fields.Float('Qty', compute='get_qty16')
    desc16_purchase = fields.Float('Purchase', default=1.25)
    desc16_sale = fields.Float('Sale', compute='get_desc16_sale')
    desc16_total = fields.Float('Total', compute='get_desc16_total')

    @api.depends('type')
    def get_qty16(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc16_qty = 4.0
            else:
                rec.desc16_qty = 2.0

    @api.depends('margin', 'desc16_purchase')
    def get_desc16_sale(self):
        for rec in self:
            rec.desc16_sale = rec.margin * rec.desc16_purchase

    @api.depends('desc16_qty', 'desc16_sale')
    def get_desc16_total(self):
        for rec in self:
            rec.desc16_total = rec.desc16_qty * rec.desc16_sale

    ########################################################################################################################

    # line 17  ==>    Epdm-111
    desc17 = fields.Char('Description', default='Epdm-111')
    desc17_qty = fields.Float('Qty', compute='get_qty17', default=2.0)
    desc17_purchase = fields.Float('Purchase', default=2)
    desc17_sale = fields.Float('Sale', compute='get_desc17_sale')
    desc17_total = fields.Float('Total', compute='get_desc17_total')

    @api.depends('type')
    def get_qty17(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc17_qty = (rec.w * 2.0) + (rec.h * 6.0)
            elif rec.type in ('four_dalfa'):
                rec.desc17_qty = (rec.w * 2.0) + (rec.h * 8.0)
            else:
                rec.desc17_qty = (rec.w * 2.0) + (rec.h * 4.0)

    @api.depends('margin', 'desc17_purchase')
    def get_desc17_sale(self):
        for rec in self:
            rec.desc17_sale = rec.margin * rec.desc17_purchase

    @api.depends('desc17_qty', 'desc17_sale')
    def get_desc17_total(self):
        for rec in self:
            rec.desc17_total = rec.desc17_qty * rec.desc17_sale

    ########################################################################################################################

    # line 18  ==>     Epdm-124
    desc18 = fields.Char('Description', default=' Epdm-124')
    desc18_qty = fields.Float('Qty', compute='get_qty18')
    desc18_purchase = fields.Float('Purchase', default=2.0)
    desc18_sale = fields.Float('Sale', compute='get_desc18_sale')
    desc18_total = fields.Float('Total', compute='get_desc18_total')

    @api.depends('type')
    def get_qty18(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc18_qty = (rec.w * 2.0) + (rec.h * 6.0)
            elif rec.type in ('four_dalfa'):
                rec.desc18_qty = (rec.w * 2.0) + (rec.h * 8.0)
            else:
                rec.desc18_qty = (rec.w * 2.0) + (rec.h * 4.0)

    @api.depends('margin', 'desc18_purchase')
    def get_desc18_sale(self):
        for rec in self:
            rec.desc18_sale = rec.margin * rec.desc18_purchase

    @api.depends('desc18_qty', 'desc18_sale')
    def get_desc18_total(self):
        for rec in self:
            rec.desc18_total = rec.desc18_qty * rec.desc18_sale

    ########################################################################################################################

    # line 19  ==>     Epdm-142
    desc19 = fields.Char('Description', default='Epdm-142')
    desc19_qty = fields.Float('Qty', compute='get_qty19')
    desc19_purchase = fields.Float('Purchase', default=2.0)
    desc19_sale = fields.Float('Sale', compute='get_desc19_sale')
    desc19_total = fields.Float('Total', compute='get_desc19_total')

    @api.depends('type')
    def get_qty19(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc19_qty = rec.h * 2.0
            else:
                rec.desc19_qty = rec.h * 4.0

    @api.depends('margin', 'desc19_purchase')
    def get_desc19_sale(self):
        for rec in self:
            rec.desc19_sale = rec.margin * rec.desc19_purchase

    @api.depends('desc19_qty', 'desc19_sale')
    def get_desc19_total(self):
        for rec in self:
            rec.desc19_total = rec.desc19_qty * rec.desc19_sale

    ########################################################################################################################

    # line 20  ==>      Forsh- 7*10
    desc20 = fields.Char('Description', default='Forsh- 7*10')
    desc20_qty = fields.Float('Qty', compute='get_qty20')
    desc20_purchase = fields.Float('Purchase', default=1.0)
    desc20_sale = fields.Float('Sale', compute='get_desc20_sale')
    desc20_total = fields.Float('Total', compute='get_desc20_total')

    @api.depends('type')
    def get_qty20(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc20_qty = (rec.w * 4.0) + (rec.h * 10.0)
            elif rec.type in ('four_dalfa'):
                rec.desc20_qty = (rec.w * 4.0) + (rec.h * 14.0)
            else:
                rec.desc20_qty = (rec.w * 4.0) + (rec.h * 8.0)

    @api.depends('margin', 'desc20_purchase')
    def get_desc20_sale(self):
        for rec in self:
            rec.desc20_sale = rec.margin * rec.desc20_purchase

    @api.depends('desc20_qty', 'desc20_sale')
    def get_desc20_total(self):
        for rec in self:
            rec.desc20_total = rec.desc20_qty * rec.desc20_sale

    ########################################################################################################################
    # line  21  ==>   مسامير تثبيت
    desc21 = fields.Char('Description', default='مسامير تثبيت')
    desc21_qty = fields.Float('Qty', default=12.0)
    desc21_purchase = fields.Float('Purchase', default=1.0)
    desc21_sale = fields.Float('Sale', compute='get_desc21_sale')
    desc21_total = fields.Float('Total', compute='get_desc21_total')

    @api.depends('margin', 'desc21_purchase')
    def get_desc21_sale(self):
        for rec in self:
            rec.desc21_sale = rec.margin * rec.desc21_purchase

    @api.depends('desc21_qty', 'desc21_sale')
    def get_desc21_total(self):
        for rec in self:
            rec.desc21_total = rec.desc21_qty * rec.desc21_sale

    ########################################################################################################################

    # line  22  ==>   بصمات
    desc22 = fields.Char('Description', default='بصمات')
    desc22_qty = fields.Float('Qty', default=12.0)
    desc22_purchase = fields.Float('Purchase', default=0.30)
    desc22_sale = fields.Float('Sale', compute='get_desc22_sale')
    desc22_total = fields.Float('Totaقفl', compute='get_desc22_total')

    @api.depends('margin', 'desc22_purchase')
    def get_desc22_sale(self):
        for rec in self:
            rec.desc22_sale = rec.margin * rec.desc22_purchase

    @api.depends('desc22_qty', 'desc22_sale')
    def get_desc22_total(self):
        for rec in self:
            rec.desc22_total = rec.desc22_qty * rec.desc22_sale

    ########################################################################################################################

    # line  23  ==>   سيلكون
    desc23 = fields.Char('Description', default='سيلكون')
    desc23_qty = fields.Float('Qty', default=2.0)
    desc23_purchase = fields.Float('Purchase', default=36.0)
    desc23_sale = fields.Float('Sale', compute='get_desc23_sale')
    desc23_total = fields.Float('Total', compute='get_desc23_total')

    @api.depends('margin', 'desc23_purchase')
    def get_desc23_sale(self):
        for rec in self:
            rec.desc23_sale = rec.margin * rec.desc23_purchase

    @api.depends('desc23_qty', 'desc23_sale')
    def get_desc23_total(self):
        for rec in self:
            rec.desc23_total = rec.desc23_qty * rec.desc23_sale

########################################################################################################################

    ###################################
    ###########    Tango   ############
    ###################################

########################################################################################################################

    # line 24  ==>   COR-0325
    desc24 = fields.Char('Description', default='COR-0325')
    desc24_qty = fields.Float('Qty', compute='get_qty24')
    desc24_purchase = fields.Float('Purchase', default=3.65)
    desc24_sale = fields.Float('Sale', compute='get_desc24_sale')
    desc24_total = fields.Float('Total', compute='get_desc24_total')

    @api.depends('type')
    def get_qty24(self):
        for rec in self:
            if rec.type in ('four_dalfa'):
                rec.desc24_qty = 20.0
            elif rec.type in ('tree_dalfa'):
                rec.desc24_qty = 16.0
            else:
                rec.desc24_qty = 12.0

    @api.depends('margin', 'desc24_purchase')
    def get_desc24_sale(self):
        for rec in self:
            rec.desc24_sale = rec.margin * rec.desc24_purchase

    @api.depends('desc24_qty', 'desc24_sale')
    def get_desc24_total(self):
        for rec in self:
            rec.desc24_total = rec.desc24_qty * rec.desc24_sale

    ########################################################################################################################
    # line 25  ==>    Plast-550
    desc25 = fields.Char('Description', default=' Plast-550')
    desc25_qty = fields.Float('Qty', compute='get_qty25')
    desc25_purchase = fields.Float('Purchase', default=0.065)
    desc25_sale = fields.Float('Sale', compute='get_desc25_sale')
    desc25_total = fields.Float('Total', compute='get_desc25_total')

    @api.depends('type')
    def get_qty25(self):
        for rec in self:
            rec.desc25_qty = ((rec.w * 2.0) + (rec.h * 2.0)) / 0.3

    @api.depends('margin', 'desc25_purchase')
    def get_desc25_sale(self):
        for rec in self:
            rec.desc25_sale = rec.margin * rec.desc25_purchase

    @api.depends('desc25_qty', 'desc25_sale')
    def get_desc25_total(self):
        for rec in self:
            rec.desc25_total = rec.desc25_qty * rec.desc25_sale

    ########################################################################################################################

    # line 26  ==>  Plast-570
    desc26 = fields.Char('Description', default='Plast-570')
    desc26_qty = fields.Float('Qty', compute='get_qty26')
    desc26_purchase = fields.Float('Purchase', default=1.0)
    desc26_sale = fields.Float('Sale', compute='get_desc26_sale')
    desc26_total = fields.Float('Total', compute='get_desc26_total')

    @api.depends('type')
    def get_qty26(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc26_qty = 24.0
            elif rec.type in ('four_dalfa'):
                rec.desc26_qty = 32.0
            else:
                rec.desc26_qty = 16.0

    @api.depends('margin', 'desc26_purchase')
    def get_desc26_sale(self):
        for rec in self:
            rec.desc26_sale = rec.margin * rec.desc26_purchase

    @api.depends('desc26_qty', 'desc26_sale')
    def get_desc26_total(self):
        for rec in self:
            rec.desc26_total = rec.desc26_qty * rec.desc26_sale

    ########################################################################################################################

    # line 27  ==>  Plast-573
    desc27 = fields.Char('Description', default='Plast-573')
    desc27_qty = fields.Float('Qty', compute='get_qty27')
    desc27_purchase = fields.Float('Purchase', default=1.0)
    desc27_sale = fields.Float('Sale', compute='get_desc27_sale')
    desc27_total = fields.Float('Total', compute='get_desc27_total')

    @api.depends('type')
    def get_qty27(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc27_qty = 12.0
            else:
                rec.desc27_qty = 8.0

    @api.depends('margin', 'desc27_purchase')
    def get_desc27_sale(self):
        for rec in self:
            rec.desc27_sale = rec.margin * rec.desc27_purchase

    @api.depends('desc27_qty', 'desc27_sale')
    def get_desc27_total(self):
        for rec in self:
            rec.desc27_total = rec.desc27_qty * rec.desc27_sale

    ########################################################################################################################

    # line 28  ==>  Plast-574
    desc28 = fields.Char('Description', default='Plast-574')
    desc28_qty = fields.Float('Qty', default=4.0)
    desc28_purchase = fields.Float('Purchase', default=1.0)
    desc28_sale = fields.Float('Sale', compute='get_desc28_sale')
    desc28_total = fields.Float('Total', compute='get_desc28_total')

    @api.depends('margin', 'desc28_purchase')
    def get_desc28_sale(self):
        for rec in self:
            rec.desc28_sale = rec.margin * rec.desc28_purchase

    @api.depends('desc28_qty', 'desc28_sale')
    def get_desc28_total(self):
        for rec in self:
            rec.desc28_total = rec.desc28_qty * rec.desc28_sale

    ########################################################################################################################

    # line 29  ==>   Zama-3004
    desc29 = fields.Char('Description', default=' Zama-3004')
    desc29_qty = fields.Float('Qty', default=2.0)
    desc29_purchase = fields.Float('Purchase', default=3.50)
    desc29_sale = fields.Float('Sale', compute='get_desc29_sale')
    desc29_total = fields.Float('Total', compute='get_desc29_total')

    @api.depends('margin', 'desc29_purchase')
    def get_desc29_sale(self):
        for rec in self:
            rec.desc29_sale = rec.margin * rec.desc29_purchase

    @api.depends('desc29_qty', 'desc29_sale')
    def get_desc29_total(self):
        for rec in self:
            rec.desc29_total = rec.desc29_qty * rec.desc29_sale

    ########################################################################################################################

    # line 30  ==>    IH-247
    desc30 = fields.Char('Description', default='IH-247')
    desc30_qty = fields.Float('Qty', default=2.0)
    desc30_purchase = fields.Float('Purchase', default=40.0)
    desc30_sale = fields.Float('Sale', compute='get_desc30_sale')
    desc30_total = fields.Float('Total', compute='get_desc30_total')

    @api.depends('margin', 'desc30_purchase')
    def get_desc30_sale(self):
        for rec in self:
            rec.desc30_sale = rec.margin * rec.desc30_purchase

    @api.depends('desc30_qty', 'desc30_sale')
    def get_desc30_total(self):
        for rec in self:
            rec.desc30_total = rec.desc30_qty * rec.desc30_sale

    ########################################################################################################################

    # line 30  ==>     3050
    desc31 = fields.Char('Description', default='3050')
    desc31_qty = fields.Float('Qty', default=1.0)
    desc31_purchase = fields.Float('Purchase', default=20.0)
    desc31_sale = fields.Float('Sale', compute='get_desc31_sale')
    desc31_total = fields.Float('Total', compute='get_desc31_total')

    @api.depends('margin', 'desc31_purchase')
    def get_desc31_sale(self):
        for rec in self:
            rec.desc31_sale = rec.margin * rec.desc31_purchase

    @api.depends('desc31_qty', 'desc31_sale')
    def get_desc31_total(self):
        for rec in self:
            rec.desc31_total = rec.desc31_qty * rec.desc31_sale

    ########################################################################################################################

    # line 32  ==>   Roll-3266-N
    desc32 = fields.Char('Description', default='Roll-3266-N')
    desc32_qty = fields.Float('Qty', compute='get_qty32')
    desc32_purchase = fields.Float('Purchase', default=12.50)
    desc32_sale = fields.Float('Sale', compute='get_desc32_sale')
    desc32_total = fields.Float('Total', compute='get_desc32_total')

    @api.depends('type')
    def get_qty32(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc32_qty = 6.0
            elif rec.type in ('four_dalfa'):
                rec.desc32_qty = 8.0
            else:
                rec.desc32_qty = 4.0

    @api.depends('margin', 'desc32_purchase')
    def get_desc32_sale(self):
        for rec in self:
            rec.desc32_sale = rec.margin * rec.desc32_purchase

    @api.depends('desc32_qty', 'desc32_sale')
    def get_desc32_total(self):
        for rec in self:
            rec.desc32_total = rec.desc32_qty * rec.desc32_sale

    ########################################################################################################################

    # line 33  ==>    Antidust-3365
    desc33 = fields.Char('Description', default='Antidust-3365')
    desc33_qty = fields.Float('Qty', compute='get_qty33')
    desc33_purchase = fields.Float('Purchase', default=5.0)
    desc33_sale = fields.Float('Sale', compute='get_desc33_sale')
    desc33_total = fields.Float('Total', compute='get_desc33_total')

    @api.depends('type')
    def get_qty33(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc33_qty = 2.0
            else:
                rec.desc33_qty = 4.0

    @api.depends('margin', 'desc33_purchase')
    def get_desc33_sale(self):
        for rec in self:
            rec.desc33_sale = rec.margin * rec.desc33_purchase

    @api.depends('desc33_qty', 'desc33_sale')
    def get_desc33_total(self):
        for rec in self:
            rec.desc33_total = rec.desc33_qty * rec.desc33_sale

    ########################################################################################################################

    # line 34  ==>     Plast-3465
    desc34 = fields.Char('Description', default='Plast-3465')
    desc34_qty = fields.Float('Qty', compute='get_qty34')
    desc34_purchase = fields.Float('Purchase', default=5.0)
    desc34_sale = fields.Float('Sale', compute='get_desc34_sale')
    desc34_total = fields.Float('Total', compute='get_desc34_total')

    @api.depends('type')
    def get_qty34(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc34_qty = 4.0
            else:
                rec.desc34_qty = 8.0

    @api.depends('margin', 'desc34_purchase')
    def get_desc34_sale(self):
        for rec in self:
            rec.desc34_sale = rec.margin * rec.desc34_purchase

    @api.depends('desc34_qty', 'desc34_sale')
    def get_desc34_total(self):
        for rec in self:
            rec.desc34_total = rec.desc34_qty * rec.desc34_sale

    ########################################################################################################################

    # line 35  ==>    Plast-3466
    desc35 = fields.Char('Description', default='Plast-3466')
    desc35_qty = fields.Float('Qty', compute='get_qty35')
    desc35_purchase = fields.Float('Purchase', default=1.25)
    desc35_sale = fields.Float('Sale', compute='get_desc35_sale')
    desc35_total = fields.Float('Total', compute='get_desc35_total')

    @api.depends('type')
    def get_qty35(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc35_qty = 6.0
            elif rec.type in ('four_dalfa'):
                rec.desc35_qty = 8.0
            else:
                rec.desc35_qty = 4.0

    @api.depends('margin', 'desc35_purchase')
    def get_desc35_sale(self):
        for rec in self:
            rec.desc35_sale = rec.margin * rec.desc35_purchase

    @api.depends('desc35_qty', 'desc35_sale')
    def get_desc35_total(self):
        for rec in self:
            rec.desc35_total = rec.desc35_qty * rec.desc35_sale

    ########################################################################################################################

    # line 36  ==>     Plast-3510
    desc36 = fields.Char('Description', default='Plast-3510')
    desc36_qty = fields.Float('Qty', default=2.0)
    desc36_purchase = fields.Float('Purchase', default=2.50)
    desc36_sale = fields.Float('Sale', compute='get_desc36_sale')
    desc36_total = fields.Float('Total', compute='get_desc36_total')

    @api.depends('margin', 'desc36_purchase')
    def get_desc36_sale(self):
        for rec in self:
            rec.desc36_sale = rec.margin * rec.desc36_purchase

    @api.depends('desc36_qty', 'desc36_sale')
    def get_desc36_total(self):
        for rec in self:
            rec.desc36_total = rec.desc36_qty * rec.desc36_sale

    ########################################################################################################################

    # line 37  ==>   Epdm-116
    desc37 = fields.Char('Description', default='Epdm-116')
    desc37_qty = fields.Float('Qty', compute='get_qty37')
    desc37_purchase = fields.Float('Purchase', default=2)
    desc37_sale = fields.Float('Sale', compute='get_desc37_sale')
    desc37_total = fields.Float('Total', compute='get_desc37_total')

    @api.depends('type')
    def get_qty37(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc37_qty = (rec.w * 4.0) + (rec.h * 6.0)
            elif rec.type in ('four_dalfa'):
                rec.desc37_qty = (rec.w * 2.0) + (rec.h * 8.0)
            else:
                rec.desc37_qty = (rec.w * 2.0) + (rec.h * 4.0)

    @api.depends('margin', 'desc37_purchase')
    def get_desc37_sale(self):
        for rec in self:
            rec.desc37_sale = rec.margin * rec.desc37_purchase

    @api.depends('desc37_qty', 'desc37_sale')
    def get_desc37_total(self):
        for rec in self:
            rec.desc37_total = rec.desc37_qty * rec.desc37_sale

    ########################################################################################################################

    # line 38  ==>   Epdm-122
    desc38 = fields.Char('Description', default='Epdm-122')
    desc38_qty = fields.Float('Qty', compute='get_qty38')
    desc38_purchase = fields.Float('Purchase', default=2)
    desc38_sale = fields.Float('Sale', compute='get_desc38_sale')
    desc38_total = fields.Float('Total', compute='get_desc38_total')

    @api.depends('type')
    def get_qty38(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc38_qty = (rec.w * 2.0) + (rec.h * 6.0)
            elif rec.type in ('four_dalfa'):
                rec.desc38_qty = (rec.w * 2.0) + (rec.h * 8.0)
            else:
                rec.desc38_qty = (rec.w * 2.0) + (rec.h * 4.0)

    @api.depends('margin', 'desc38_purchase')
    def get_desc38_sale(self):
        for rec in self:
            rec.desc38_sale = rec.margin * rec.desc38_purchase

    @api.depends('desc38_qty', 'desc38_sale')
    def get_desc38_total(self):
        for rec in self:
            rec.desc38_total = rec.desc38_qty * rec.desc38_sale

    ########################################################################################################################

    # line 39  ==>   Forsh- 7*7
    desc39 = fields.Char('Description', default='Forsh- 7*7')
    desc39_qty = fields.Float('Qty', compute='get_qty39')
    desc39_purchase = fields.Float('Purchase', default=1)
    desc39_sale = fields.Float('Sale', compute='get_desc39_sale')
    desc39_total = fields.Float('Total', compute='get_desc39_total')

    @api.depends('type')
    def get_qty39(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc39_qty = (rec.w * 4.0) + (rec.h * 8.0)
            elif rec.type in ('four_dalfa'):
                rec.desc39_qty = (rec.w * 4.0) + (rec.h * 10.0)
            else:
                rec.desc39_qty = (rec.w * 4.0) + (rec.h * 6.0)

    @api.depends('margin', 'desc39_purchase')
    def get_desc39_sale(self):
        for rec in self:
            rec.desc39_sale = rec.margin * rec.desc39_purchase

    @api.depends('desc39_qty', 'desc39_sale')
    def get_desc39_total(self):
        for rec in self:
            rec.desc39_total = rec.desc39_qty * rec.desc39_sale

    ########################################################################################################################

    # line 40  ==>   مسامير تثبيت
    desc40 = fields.Char('Description', default='مسامير تثبيت')
    desc40_qty = fields.Float('Qty', compute='get_qty40')
    desc40_purchase = fields.Float('Purchase', default=1.0)
    desc40_sale = fields.Float('Sale', compute='get_desc40_sale')
    desc40_total = fields.Float('Total', compute='get_desc40_total')

    @api.depends('type')
    def get_qty40(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc40_qty = 12.0
            else:
                rec.desc40_qty = 16.0

    @api.depends('margin', 'desc40_purchase')
    def get_desc40_sale(self):
        for rec in self:
            rec.desc40_sale = rec.margin * rec.desc40_purchase

    @api.depends('desc40_qty', 'desc40_sale')
    def get_desc40_total(self):
        for rec in self:
            rec.desc40_total = rec.desc40_qty * rec.desc40_sale

    ########################################################################################################################

    # line 41  ==>   بصمات
    desc41 = fields.Char('Description', default='بصمات')
    desc41_qty = fields.Float('Qty', compute='get_qty41')
    desc41_purchase = fields.Float('Purchase', default=0.30)
    desc41_sale = fields.Float('Sale', compute='get_desc41_sale')
    desc41_total = fields.Float('Total', compute='get_desc41_total')

    @api.depends('type')
    def get_qty41(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc41_qty = 12.0
            else:
                rec.desc41_qty = 16.0

    @api.depends('margin', 'desc41_purchase')
    def get_desc41_sale(self):
        for rec in self:
            rec.desc41_sale = rec.margin * rec.desc41_purchase

    @api.depends('desc41_qty', 'desc41_sale')
    def get_desc41_total(self):
        for rec in self:
            rec.desc41_total = rec.desc41_qty * rec.desc41_sale

    ########################################################################################################################

    # line 42  ==>   السيلكون
    desc42 = fields.Char('Description', default='السيلكون')
    desc42_qty = fields.Float('Qty', compute='get_qty42')
    desc42_purchase = fields.Float('Purchase', default=36.0)
    desc42_sale = fields.Float('Sale', compute='get_desc42_sale')
    desc42_total = fields.Float('Total', compute='get_desc42_total')

    @api.depends('type')
    def get_qty42(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc42_qty = 2.0
            else:
                rec.desc42_qty = 4.0

    @api.depends('margin', 'desc42_purchase')
    def get_desc42_sale(self):
        for rec in self:
            rec.desc42_sale = rec.margin * rec.desc42_purchase

    @api.depends('desc42_qty', 'desc42_sale')
    def get_desc42_total(self):
        for rec in self:
            rec.desc42_total = rec.desc42_qty * rec.desc42_sale

########################################################################################################################

    #####################################
    ###########    PS 9600   ############
    #####################################

########################################################################################################################

    # line 43  ==>    كورنر حلق وضلفه
    desc43 = fields.Char('Description', default='كورنر حلق وضلفه')
    desc43_qty = fields.Float('Qty', compute='get_qty43')
    desc43_purchase = fields.Float('Purchase', default=4.0)
    desc43_sale = fields.Float('Sale', compute='get_desc43_sale')
    desc43_total = fields.Float('Total', compute='get_desc43_total')

    @api.depends('type')
    def get_qty43(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc43_qty = 12.0
            elif rec.type in ('tree_dalfa'):
                rec.desc43_qty = 16.0
            else:
                rec.desc43_qty = 20.0

    @api.depends('margin', 'desc43_purchase')
    def get_desc43_sale(self):
        for rec in self:
            rec.desc43_sale = rec.margin * rec.desc43_purchase

    @api.depends('desc43_qty', 'desc43_sale')
    def get_desc43_total(self):
        for rec in self:
            rec.desc43_total = rec.desc43_qty * rec.desc43_sale

########################################################################################################################

    # line 44  ==>     زوايا تجميع
    desc44 = fields.Char('Description', default='زوايا تجميع')
    desc44_qty = fields.Float('Qty', compute='get_qty44')
    desc44_purchase = fields.Float('Purchase', default=1.0)
    desc44_sale = fields.Float('Sale', compute='get_desc44_sale')
    desc44_total = fields.Float('Total', compute='get_desc44_total')

    @api.depends('type')
    def get_qty44(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc44_qty = 16.0
            elif rec.type in ('tree_dalfa'):
                rec.desc44_qty = 24.0
            else:
                rec.desc44_qty = 32.0

    @api.depends('margin', 'desc44_purchase')
    def get_desc44_sale(self):
        for rec in self:
            rec.desc44_sale = rec.margin * rec.desc44_purchase

    @api.depends('desc44_qty', 'desc44_sale')
    def get_desc44_total(self):
        for rec in self:
            rec.desc44_total = rec.desc44_qty * rec.desc44_sale


########################################################################################################################

    # line 45  ==>     زوايا تجميع
    desc45 = fields.Char('Description', default='زوايا تجميع')
    desc45_qty = fields.Float('Qty', compute='get_qty45')
    desc45_purchase = fields.Float('Purchase', default=1.0)
    desc45_sale = fields.Float('Sale', compute='get_desc45_sale')
    desc45_total = fields.Float('Total', compute='get_desc45_total')

    @api.depends('type')
    def get_qty45(self):
        for rec in self:
            rec.desc45_qty = 4.0

    @api.depends('margin', 'desc45_purchase')
    def get_desc45_sale(self):
        for rec in self:
            rec.desc45_sale = rec.margin * rec.desc45_purchase

    @api.depends('desc45_qty', 'desc45_sale')
    def get_desc45_total(self):
        for rec in self:
            rec.desc45_total = rec.desc45_qty * rec.desc45_sale


########################################################################################################################

    # line 46  ==>     زوايا تجميع
    desc46 = fields.Char('Description', default='زوايا تجميع')
    desc46_qty = fields.Float('Qty', compute='get_qty46')
    desc46_purchase = fields.Float('Purchase', default=1.0)
    desc46_sale = fields.Float('Sale', compute='get_desc46_sale')
    desc46_total = fields.Float('Total', compute='get_desc46_total')

    @api.depends('type')
    def get_qty46(self):
        for rec in self:
            rec.desc46_qty = 4.0

    @api.depends('margin', 'desc46_purchase')
    def get_desc46_sale(self):
        for rec in self:
            rec.desc46_sale = rec.margin * rec.desc46_purchase

    @api.depends('desc46_qty', 'desc46_sale')
    def get_desc46_total(self):
        for rec in self:
            rec.desc46_total = rec.desc46_qty * rec.desc46_sale

########################################################################################################################

    # line 47  ==>      زوايا تجميع البار
    desc47 = fields.Char('Description', default=' زوايا تجميع البار ')
    desc47_qty = fields.Float('Qty', compute='get_qty47')
    desc47_purchase = fields.Float('Purchase', default=1.0)
    desc47_sale = fields.Float('Sale', compute='get_desc47_sale')
    desc47_total = fields.Float('Total', compute='get_desc47_total')

    @api.depends('type')
    def get_qty47(self):
        for rec in self:
            rec.desc47_qty = 4.0

    @api.depends('margin', 'desc47_purchase')
    def get_desc47_sale(self):
        for rec in self:
            rec.desc47_sale = rec.margin * rec.desc47_purchase

    @api.depends('desc47_qty', 'desc47_sale')
    def get_desc47_total(self):
        for rec in self:
            rec.desc47_total = rec.desc47_qty * rec.desc47_sale

########################################################################################################################

    # line 48  ==>      مصد بارز
    desc48 = fields.Char('Description', default='  مصد بارز  ')
    desc48_qty = fields.Float('Qty', compute='get_qty48')
    desc48_purchase = fields.Float('Purchase', default=3.50)
    desc48_sale = fields.Float('Sale', compute='get_desc48_sale')
    desc48_total = fields.Float('Total', compute='get_desc48_total')

    @api.depends('type')
    def get_qty48(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc48_qty = 2.0
            elif rec.type in ('tree_dalfa'):
                rec.desc48_qty = 3.0
            else:
                rec.desc48_qty = 4.0

    @api.depends('margin', 'desc48_purchase')
    def get_desc48_sale(self):
        for rec in self:
            rec.desc48_sale = rec.margin * rec.desc48_purchase

    @api.depends('desc48_qty', 'desc48_sale')
    def get_desc48_total(self):
        for rec in self:
            rec.desc48_total = rec.desc48_qty * rec.desc48_sale

########################################################################################################################

    # line 49  ==>     سكاك برتغالى
    desc49 = fields.Char('Description', default='  سكاك برتغالى  ')
    desc49_qty = fields.Float('Qty', compute='get_qty49')
    desc49_purchase = fields.Float('Purchase', default=40.0)
    desc49_sale = fields.Float('Sale', compute='get_desc49_sale')
    desc49_total = fields.Float('Total', compute='get_desc49_total')

    @api.depends('type')
    def get_qty49(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc49_qty = 2.0
            elif rec.type in ('tree_dalfa'):
                rec.desc49_qty = 3.0
            else:
                rec.desc49_qty = 4.0

    @api.depends('margin', 'desc49_purchase')
    def get_desc49_sale(self):
        for rec in self:
            rec.desc49_sale = rec.margin * rec.desc49_purchase

    @api.depends('desc49_qty', 'desc49_sale')
    def get_desc49_total(self):
        for rec in self:
            rec.desc49_total = rec.desc49_qty * rec.desc49_sale

########################################################################################################################

    # line 50  ==>      مقبض يد
    desc50 = fields.Char('Description', default=' مقبض يد ')
    desc50_qty = fields.Float('Qty', compute='get_qty50')
    desc50_purchase = fields.Float('Purchase', default=20.0)
    desc50_sale = fields.Float('Sale', compute='get_desc50_sale')
    desc50_total = fields.Float('Total', compute='get_desc50_total')

    @api.depends('type')
    def get_qty50(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc50_qty = 1.0
            else:
                rec.desc50_qty = 2.0

    @api.depends('margin', 'desc50_purchase')
    def get_desc50_sale(self):
        for rec in self:
            rec.desc50_sale = rec.margin * rec.desc50_purchase

    @api.depends('desc50_qty', 'desc50_sale')
    def get_desc50_total(self):
        for rec in self:
            rec.desc50_total = rec.desc50_qty * rec.desc50_sale

########################################################################################################################

    # line 51  ==>    عجل
    desc51 = fields.Char('Description', default=' عجل ')
    desc51_qty = fields.Float('Qty', compute='get_qty51')
    desc51_purchase = fields.Float('Purchase', default=13.75)
    desc51_sale = fields.Float('Sale', compute='get_desc51_sale')
    desc51_total = fields.Float('Total', compute='get_desc51_total')

    @api.depends('type')
    def get_qty51(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc51_qty = 4.0
            elif rec.type in ('tree_dalfa'):
                rec.desc51_qty = 6.0
            else:
                rec.desc51_qty = 8.0

    @api.depends('margin', 'desc51_purchase')
    def get_desc51_sale(self):
        for rec in self:
            rec.desc51_sale = rec.margin * rec.desc51_purchase

    @api.depends('desc51_qty', 'desc51_sale')
    def get_desc51_total(self):
        for rec in self:
            rec.desc51_total = rec.desc51_qty * rec.desc51_sale

########################################################################################################################

    # line 52  ==>     طفم تسكيك
    desc52 = fields.Char('Description', default=' طفم تسكيك ')
    desc52_qty = fields.Float('Qty', compute='get_qty52')
    desc52_purchase = fields.Float('Purchase', default=5.0)
    desc52_sale = fields.Float('Sale', compute='get_desc52_sale')
    desc52_total = fields.Float('Total', compute='get_desc52_total')

    @api.depends('type')
    def get_qty52(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc52_qty = 1.0
            else:
                rec.desc52_qty = 2.0

    @api.depends('margin', 'desc52_purchase')
    def get_desc52_sale(self):
        for rec in self:
            rec.desc52_sale = rec.margin * rec.desc52_purchase

    @api.depends('desc52_qty', 'desc52_sale')
    def get_desc52_total(self):
        for rec in self:
            rec.desc52_total = rec.desc52_qty * rec.desc52_sale

########################################################################################################################

    # line 53  ==>    مانع اتربه
    desc53 = fields.Char('Description', default=' مانع اتربه ')
    desc53_qty = fields.Float('Qty', compute='get_qty53')
    desc53_purchase = fields.Float('Purchase', default=5.0)
    desc53_sale = fields.Float('Sale', compute='get_desc53_sale')
    desc53_total = fields.Float('Total', compute='get_desc53_total')

    @api.depends('type')
    def get_qty53(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc53_qty = 2.0
            else:
                rec.desc53_qty = 4.0

    @api.depends('margin', 'desc53_purchase')
    def get_desc53_sale(self):
        for rec in self:
            rec.desc53_sale = rec.margin * rec.desc53_purchase

    @api.depends('desc53_qty', 'desc53_sale')
    def get_desc53_total(self):
        for rec in self:
            rec.desc53_total = rec.desc53_qty * rec.desc53_sale


########################################################################################################################

    # line 54  ==>    طقم غطاء للسكينه
    desc54 = fields.Char('Description', default='  طقم غطاء للسكينه ')
    desc54_qty = fields.Float('Qty', compute='get_qty54')
    desc54_purchase = fields.Float('Purchase', default=5.0)
    desc54_sale = fields.Float('Sale', compute='get_desc54_sale')
    desc54_total = fields.Float('Total', compute='get_desc54_total')

    @api.depends('type')
    def get_qty54(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc54_qty = 1.0
            else:
                rec.desc54_qty = 2.0

    @api.depends('margin', 'desc54_purchase')
    def get_desc54_sale(self):
        for rec in self:
            rec.desc54_sale = rec.margin * rec.desc54_purchase

    @api.depends('desc54_qty', 'desc54_sale')
    def get_desc54_total(self):
        for rec in self:
            rec.desc54_total = rec.desc54_qty * rec.desc54_sale

########################################################################################################################

    # line 55  ==>  جراب مطر
    desc55 = fields.Char('Description', default=' جراب مطر ')
    desc55_qty = fields.Float('Qty', compute='get_qty55')
    desc55_purchase = fields.Float('Purchase', default=2.50)
    desc55_sale = fields.Float('Sale', compute='get_desc55_sale')
    desc55_total = fields.Float('Total', compute='get_desc55_total')

    @api.depends('type')
    def get_qty55(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc55_qty = 2.0
            else:
                rec.desc55_qty = 3.0

    @api.depends('margin', 'desc55_purchase')
    def get_desc55_sale(self):
        for rec in self:
            rec.desc55_sale = rec.margin * rec.desc55_purchase

    @api.depends('desc55_qty', 'desc55_sale')
    def get_desc55_total(self):
        for rec in self:
            rec.desc55_total = rec.desc55_qty * rec.desc55_sale



########################################################################################################################

    # line 56  ==>   كاوتش زجاج خارجى - GT 0111
    desc56 = fields.Char('Description', default=' كاوتش زجاج خارجى - GT 0111 ')
    desc56_qty = fields.Float('Qty', compute='get_qty56')
    desc56_purchase = fields.Float('Purchase', default=2.0)
    desc56_sale = fields.Float('Sale', compute='get_desc56_sale')
    desc56_total = fields.Float('Total', compute='get_desc56_total')

    @api.depends('type')
    def get_qty56(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc56_qty = (rec.w * 2.0) + (rec.h * 6.0)
            elif rec.type in ('four_dalfa'):
                rec.desc56_qty = (rec.w * 2.0) + (rec.h * 8.0)
            else:
                rec.desc56_qty = (rec.w * 2.0) + (rec.h * 4.0)

    @api.depends('margin', 'desc56_purchase')
    def get_desc56_sale(self):
        for rec in self:
            rec.desc56_sale = rec.margin * rec.desc56_purchase

    @api.depends('desc56_qty', 'desc56_sale')
    def get_desc56_total(self):
        for rec in self:
            rec.desc56_total = rec.desc56_qty * rec.desc56_sale

########################################################################################################################

    # line 57  ==>    كاوتش زجاج  - GT 0122
    desc57 = fields.Char('Description', default='  كاوتش زجاج  - GT 0122 ')
    desc57_qty = fields.Float('Qty', compute='get_qty57')
    desc57_purchase = fields.Float('Purchase', default=2.0)
    desc57_sale = fields.Float('Sale', compute='get_desc57_sale')
    desc57_total = fields.Float('Total', compute='get_desc57_total')

    @api.depends('type')
    def get_qty57(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc57_qty = (rec.w * 2.0) + (rec.h * 6.0)
            elif rec.type in ('four_dalfa'):
                rec.desc57_qty = (rec.w * 2.0) + (rec.h * 8.0)
            else:
                rec.desc57_qty = (rec.w * 2.0) + (rec.h * 4.0)

    @api.depends('margin', 'desc57_purchase')
    def get_desc57_sale(self):
        for rec in self:
            rec.desc57_sale = rec.margin * rec.desc57_purchase

    @api.depends('desc57_qty', 'desc57_sale')
    def get_desc57_total(self):
        for rec in self:
            rec.desc57_total = rec.desc57_qty * rec.desc57_sale


########################################################################################################################

    # line 58  ==>    Forsh- 7*10
    desc58 = fields.Char('Description', default='Forsh- 7*10')
    desc58_qty = fields.Float('Qty', compute='get_qty58')
    desc58_purchase = fields.Float('Purchase', default=1.0)
    desc58_sale = fields.Float('Sale', compute='get_desc58_sale')
    desc58_total = fields.Float('Total', compute='get_desc58_total')

    @api.depends('type')
    def get_qty58(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc58_qty = (rec.w * 4.0) + (rec.h * 9.0)
            elif rec.type in ('four_dalfa'):
                rec.desc58_qty = (rec.w * 4.0) + (rec.h * 12.0)
            else:
                rec.desc58_qty = (rec.w * 4.0) + (rec.h * 6.0)

    @api.depends('margin', 'desc58_purchase')
    def get_desc58_sale(self):
        for rec in self:
            rec.desc58_sale = rec.margin * rec.desc58_purchase

    @api.depends('desc58_qty', 'desc58_sale')
    def get_desc58_total(self):
        for rec in self:
            rec.desc58_total = rec.desc58_qty * rec.desc58_sale


########################################################################################################################

    # line 59  ==>   مسامير تثبيت
    desc59 = fields.Char('Description', default='مسامير تثبيت')
    desc59_qty = fields.Float('Qty', compute='get_qty59')
    desc59_purchase = fields.Float('Purchase', default=1.0)
    desc59_sale = fields.Float('Sale', compute='get_desc59_sale')
    desc59_total = fields.Float('Total', compute='get_desc59_total')

    @api.depends('type')
    def get_qty59(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc59_qty = 12.0
            else:
                rec.desc59_qty = 16.0

    @api.depends('margin', 'desc59_purchase')
    def get_desc59_sale(self):
        for rec in self:
            rec.desc59_sale = rec.margin * rec.desc59_purchase

    @api.depends('desc59_qty', 'desc59_sale')
    def get_desc59_total(self):
        for rec in self:
            rec.desc59_total = rec.desc59_qty * rec.desc59_sale

########################################################################################################################

    # line 60  ==>   بصمات
    desc60 = fields.Char('Description', default='بصمات')
    desc60_qty = fields.Float('Qty', compute='get_qty60')
    desc60_purchase = fields.Float('Purchase', default=0.30)
    desc60_sale = fields.Float('Sale', compute='get_desc60_sale')
    desc60_total = fields.Float('Total', compute='get_desc60_total')

    @api.depends('type')
    def get_qty60(self):
        for rec in self:
            if rec.type in ('four_dalfa'):
                rec.desc60_qty = 16.0
            else:
                rec.desc60_qty = 12.0

    @api.depends('margin', 'desc60_purchase')
    def get_desc60_sale(self):
        for rec in self:
            rec.desc60_sale = rec.margin * rec.desc60_purchase

    @api.depends('desc60_qty', 'desc60_sale')
    def get_desc60_total(self):
        for rec in self:
            rec.desc60_total = rec.desc60_qty * rec.desc60_sale

########################################################################################################################

    # line 61  ==>   السيلكون
    desc61 = fields.Char('Description', default='السيلكون')
    desc61_qty = fields.Float('Qty', compute='get_qty61')
    desc61_purchase = fields.Float('Purchase', default=36.0)
    desc61_sale = fields.Float('Sale', compute='get_desc61_sale')
    desc61_total = fields.Float('Total', compute='get_desc61_total')

    @api.depends('type')
    def get_qty61(self):
        for rec in self:
            if rec.type in ('two_dalfa'):
                rec.desc61_qty = 2.0
            else:
                rec.desc61_qty = 4.0

    @api.depends('margin', 'desc61_purchase')
    def get_desc61_sale(self):
        for rec in self:
            rec.desc61_sale = rec.margin * rec.desc61_purchase

    @api.depends('desc61_qty', 'desc61_sale')
    def get_desc61_total(self):
        for rec in self:
            rec.desc61_total = rec.desc61_qty * rec.desc61_sale

    ########################################################################################################################


########################################################################################################################

    @api.depends('profile_types', 'desc1_total', 'desc2_total', 'desc3_total', 'desc4_total', 'desc5_total',
                 'desc6_total', 'desc7_total', 'desc8_total', 'desc9_total', 'desc10_total', 'desc11_total',
                 'desc12_total', 'desc13_total', 'desc14_total', 'desc15_total', 'desc16_total', 'desc17_total',
                 'desc18_total', 'desc19_total', 'desc20_total', 'desc21_total', 'desc22_total', 'desc23_total',
                 'desc24_total', 'desc25_total', 'desc26_total', 'desc27_total', 'desc28_total', 'desc29_total',
                 'desc30_total', 'desc31_total', 'desc32_total', 'desc33_total', 'desc34_total', 'desc35_total',
                 'desc36_total', 'desc37_total', 'desc38_total', 'desc39_total', 'desc40_total', 'desc41_total',
                 'desc42_total', 'desc43_total', 'desc44_total', 'desc45_total', 'desc46_total', 'desc47_total',
                 'desc48_total', 'desc49_total', 'desc50_total', 'desc51_total', 'desc52_total', 'desc53_total',
                 'desc54_total', 'desc55_total', 'desc56_total', 'desc57_total', 'desc58_total' , 'desc59_total',
                 'desc60_total', 'desc61_total'
                 )
    def get_total(self):
        for rec in self:
            if rec.profile_types == 'jumbo':
                rec.total = rec.desc1_total + rec.desc2_total + rec.desc3_total + rec.desc4_total + rec.desc5_total + rec.desc6_total + rec.desc7_total + rec.desc8_total + rec.desc9_total + rec.desc10_total + rec.desc11_total + rec.desc12_total + rec.desc13_total + rec.desc14_total + rec.desc15_total + rec.desc16_total + rec.desc17_total + rec.desc18_total + rec.desc19_total + rec.desc20_total + rec.desc21_total + rec.desc22_total + rec.desc23_total
            elif rec.profile_types == 'tango':
                rec.total = rec.desc24_total + rec.desc25_total + rec.desc26_total + rec.desc27_total + rec.desc28_total + rec.desc29_total + rec.desc30_total + rec.desc31_total + rec.desc32_total + rec.desc33_total + rec.desc34_total + rec.desc35_total + rec.desc36_total + rec.desc37_total + rec.desc38_total + rec.desc39_total + rec.desc40_total + rec.desc41_total + rec.desc42_total
            elif rec.profile_types == 'ps_9600':
                rec.total = rec.desc43_total + rec.desc44_total + rec.desc45_total + rec.desc46_total + rec.desc47_total + rec.desc48_total + rec.desc49_total + rec.desc50_total + rec.desc51_total + rec.desc52_total + rec.desc53_total + rec.desc54_total + rec.desc55_total + rec.desc56_total + rec.desc57_total + rec.desc58_total + rec.desc59_total + rec.desc60_total + rec.desc61_total
            else:
                rec.total = 0.0
