
from odoo import models,fields,api

class QuotationTemplate(models.Model):
    _name = 'quotation.template'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']

    name = fields.Char()
    notes = fields.Text()
    quotaion_id = fields.One2many('quotation.template.line', 'quotaion_ids')


class QuotaionLine(models.Model):
    _name = 'quotation.template.line'

    quotaion_ids = fields.Many2one('quotation.template')
    seq = fields.Integer()
    code = fields.Integer('Code')
    unit_loc = fields.Char('Unit Location')
    system_id = fields.Many2one('system.template')
    profile_type_ids = fields.Many2one('profile.type', domain="[('system_ids','=',system_id)]")
    profile_color_ids = fields.Many2one('paints', domain="[('system_id','=',system_id)]")
    dim_w = fields.Float('W',  related='system_id.w')
    dim_h = fields.Float('H', related='system_id.h')
    area = fields.Float('Area', compute='get_area')
    qty = fields.Integer('Qty')
    total = fields.Float('Total Area', compute='get_total')
    extra = fields.Float('Extra')
    vertical = fields.Float('Mullion Vertical')
    horizontal = fields.Float('Mullion Horizontal')
    glass_type = fields.Selection([
        ('single_glass', ' مفرد '),
        ('double_glass', ' مزدوج ')],
        string='Glass Type', default="single_glass")
    glass_color_id = fields.Many2one('glass.color',string='Glass Color' , domain="[('glass_type','=',glass_type)]")
    thickness = fields.Selection([
        ('six_mm', ' 6 مم '),
        ('twelve_mm', ' 12 مم '),
        ('eighteen_mm', ' 18 مم '),
        ('laminitude', ' ﻻمينيتد ')
    ],string='Thickness', default="six_mm")
    extra_id = fields.Many2one('glass', domain="[('glass_type','=',glass_type)]")

    unit = fields.Float('Unit', compute='get_unit', digits=(16,0))
    total_unit = fields.Float('Total', compute='get_unit_total', digits=(16,0))
    fly_screen = fields.Selection([
        ('without', ' بدون '),
        ('tractor', ' جرار '),
        ('only_you_see', ' تراك فقط ')
    ],
        string='Fly Screen', default="")
    unit_fly = fields.Float('Unit',  compute='get_unit_fly_screen', digits=(16,0))
    total_fly = fields.Float('Total', compute='get_unit_fly_total', digits=(16,0))

    design = fields.Binary('Design')

    @api.depends('dim_w', 'dim_h')
    def get_area(self):
        for rec in self:
            rec.area = rec.dim_w * rec.dim_h

    @api.depends('area', 'qty')
    def get_total(self):
        for rec in self:
            rec.total = rec.area * int(rec.qty)

    @api.depends('system_id','profile_color_ids','glass_color_id')
    def get_unit(self):
        for rec in self:
            rec.unit = (rec.system_id.accessories_id.total) + (rec.system_id.sector_id.total) + (rec.profile_color_ids.total) + (rec.glass_color_id.total) + (rec.extra_id.total)


    @api.depends('system_id', 'profile_color_ids', 'glass_color_id', 'fly_screen')
    def get_unit_fly_screen(self):
        for rec in self:
            if rec.fly_screen == 'tractor':
                rec.unit_fly = (rec.system_id.accessories_id.total) + (
                    rec.system_id.sector_id.total) + (rec.profile_color_ids.total) + (rec.glass_color_id.total) + (
                               rec.extra_id.total) + (rec.system_id.fly_id.total) + (rec.system_id.fly_accessor_id.total)
            else:
                rec.unit_fly = (rec.system_id.accessories_id.total) + (rec.system_id.sector_id.total) + (
                    rec.profile_color_ids.total) + (rec.glass_color_id.total) + (rec.extra_id.total)

    @api.depends('unit','qty')
    def get_unit_total(self):
        for rec in self:
            rec.total_unit = rec.unit * rec.qty

    @api.depends('unit_fly', 'qty')
    def get_unit_fly_total(self):
        for rec in self:
            rec.total_fly = rec.unit_fly * rec.qty

