
from odoo import models,fields,api
from datetime import datetime, timedelta



class System(models.Model):
    _name = 'system.template'

    name = fields.Char()
    w = fields.Float('W')
    h = fields.Float('H')
    accessories_id = fields.Many2one('accessories')
    sector_id = fields.Many2one('sectors')
    price_id = fields.Many2one('glass.price')
    fly_id = fields.Many2one('fly.screen', string='Fly Screen Sectors')
    fly_accessor_id = fields.Many2one('fly.accessor',string="Fly Screen Accessories")




