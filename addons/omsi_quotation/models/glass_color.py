
from odoo import models,fields,api

class GlassColor(models.Model):
    _name = 'glass.color'
    _rec_name = 'desc'

    name = fields.Many2one('system.template')
    profile_type_ids = fields.Many2one('profile.type', domain="[('system_ids','=', name)]")
    margin = fields.Float('Margin')
    glass_type = fields.Selection([
        ('single_glass', ' مفرد '),
        ('double_glass', ' مزدوج ')],
        string='Glass Type', default="single_glass")
    w = fields.Float('W', related='name.w', readonly=False)
    h = fields.Float('H', related='name.h', readonly=False)
    qty = fields.Float('Qty', compute='get_qty')
    desc = fields.Char('Description')
    purchase_price = fields.Float('Purchase Price', digits=(16,3))
    sale_price = fields.Float('Sale Price', compute='get_sale', digits=(16,3))
    total = fields.Float('Total', compute='get_total', digits=(16,3))

    @api.depends('w','h')
    def get_qty(self):
        for rec in self:
            rec.qty = rec.w * rec.h

    @api.depends('purchase_price','margin')
    def get_sale(self):
        for rec in self:
            rec.sale_price = rec.purchase_price * rec.margin

    @api.depends('sale_price', 'qty','name','margin')
    def get_total(self):
        for rec in self:
            if rec.glass_type in ('single_glass'):
                rec.total = rec.sale_price * rec.qty
            else:
                rec.total = (rec.sale_price * rec.qty) + ((rec.name.price_id.clear_glass_5) * rec.margin) + ((rec.name.price_id.double * 6) * rec.margin)




