
from odoo import models,fields,api

class ProfileColor(models.Model):
    _name = 'profile.color'

    name = fields.Char()
    color_price = fields.Float()
    profile_type_ids = fields.Many2many('profile.type')