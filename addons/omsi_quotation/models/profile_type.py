
from odoo import models,fields,api

class ProfileType(models.Model):
    _name = 'profile.type'

    name = fields.Char()
    system_ids = fields.Many2many('system.template', required=True)
