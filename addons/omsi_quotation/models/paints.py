

from odoo import models,fields,api

class Paints(models.Model):
    _name = 'paints'

    name = fields.Char()
    sector_id = fields.Many2one('sectors', string='Sector')
    price = fields.Float(default=10.0, digits=(16,3))
    margin = fields.Float(default=1.90)
    system_id = fields.Many2one('system.template')
    fly_id = fields.Many2one('fly.screen', string='Fly Screen')
    profile_type_ids = fields.Many2many('profile.type')

########################################################################################################################


    qty = fields.Float('Qty', compute="get_qty")
    purchase = fields.Float('Purchase')
    sale = fields.Float('Sale', compute='get_sale')
    total = fields.Float('Total', compute='get_total')

    @api.depends('sector_id')
    def get_qty(self):
        for rec in self:
            if rec.sector_id:
                rec.qty = ( (rec.sector_id.total + rec.fly_id.total) / rec.margin ) / rec.sector_id.kilo_rate
            else:
                rec.qty = 1

    @api.depends('margin', 'purchase')
    def get_sale(self):
        for rec in self:
            rec.sale = rec.margin * rec.price
    #
    @api.depends('qty', 'sale')
    def get_total(self):
        for rec in self:
            rec.total = rec.qty * rec.sale