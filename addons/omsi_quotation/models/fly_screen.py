
from odoo import models,fields,api

class FlyScreen(models.Model):
    _name = 'fly.screen'
    _rec_name = 'sector_name'

    sector_name = fields.Char('Fly Screen Sector')
    name = fields.Many2one('system.template')
    profile_type_ids = fields.Many2one('profile.type', domain="[('system_ids','=', name)]")
    margin = fields.Float('Margin', default=1.90)
    quot_template = fields.Many2one('quotation.template', string='Quotation Template')
    type = fields.Selection([
        ('two_dalfa', ' جرار 2 ضلفه '),
        ('tree_dalfa', ' جرار 3 ضلفة '),
        ('four_dalfa', ' جرار 4 ضلفة ')],
        string='Design', required=True, default="two_dalfa")
    profile_types = fields.Selection([
        ('jumbo', ' جامبو '),
        ('tango', ' تانجو ')],
        string='Profile', required=True, default="jumbo")
    kilo_rate = fields.Float('Kilo Rate', default=58.71)
    w = fields.Float('W', related='name.w', readonly=False)
    h = fields.Float('H', related='name.h', readonly=False)
    total = fields.Float('Total', compute='get_total')

########################################################################################################################
    ###################################
    ###########    Jumbo   ############
    ###################################


    # line  1  ==>       Fly Screen Frame K 2210

    desc1 = fields.Char('Description',default='Fly Screen Frame K 2210')
    desc1_qty = fields.Float('Qty', compute="get_qty1")
    desc1_purchase = fields.Float('Purchase', compute='get_desc1_purchase')
    desc1_sale = fields.Float('Sale', compute='get_desc1_sale')
    desc1_weight = fields.Float('Weight', default= 0.362,digits=(16,3))
    desc1_total = fields.Float('Total' , compute='get_desc1_total')

    @api.depends('type')
    def get_qty1(self):
        for rec in self:
            rec.desc1_qty = ((rec.w * 2.0) + (rec.h * 2.0)) * 1.10

    @api.depends('kilo_rate', 'desc1_weight')
    def get_desc1_purchase(self):
        for rec in self:
            rec.desc1_purchase = rec.desc1_weight * rec.kilo_rate

    @api.depends('margin', 'desc1_purchase')
    def get_desc1_sale(self):
        for rec in self:
            rec.desc1_sale = rec.margin * rec.desc1_purchase

    @api.depends('desc1_qty', 'desc1_sale')
    def get_desc1_total(self):
        for rec in self:
            rec.desc1_total = rec.desc1_qty * rec.desc1_sale



########################################################################################################################
    # line  2  ==>        Screen Sash Alu 007

    desc2 = fields.Char('Description', default='Screen Sash Alu 007')
    desc2_qty = fields.Float('Qty', compute="get_qty2")
    desc2_purchase = fields.Float('Purchase', compute='get_desc2_purchase')
    desc2_sale = fields.Float('Sale', compute='get_desc2_sale')
    desc2_weight = fields.Float('Weight', default= 0.581,digits=(16,3))
    desc2_total = fields.Float('Total' , compute='get_desc2_total')

    @api.depends('type')
    def get_qty2(self):
        for rec in self:
            if rec.type in ('four_dalfa'):
                rec.desc2_qty = ((rec.w * 1.0) + (rec.h * 4.0)) * 1.10
            else:
                rec.desc2_qty = ((rec.w * 1.0) + (rec.h * 2.0)) * 1.10

    @api.depends('kilo_rate', 'desc2_weight')
    def get_desc2_purchase(self):
        for rec in self:
            rec.desc2_purchase = rec.desc2_weight * rec.kilo_rate

    @api.depends('margin', 'desc2_purchase')
    def get_desc2_sale(self):
        for rec in self:
            rec.desc2_sale = rec.margin * rec.desc2_purchase

    @api.depends('desc2_qty', 'desc2_sale')
    def get_desc2_total(self):
        for rec in self:
            rec.desc2_total = rec.desc2_qty * rec.desc2_sale

########################################################################################################################
    # line  3  ==>         Fly Screen Sash  Calypso K2281

    desc3 = fields.Char('Description', default='Fly Screen Sash  Calypso K2281')
    desc3_qty = fields.Float('Qty', compute="get_qty3")
    desc3_purchase = fields.Float('Purchase', compute='get_desc3_purchase')
    desc3_sale = fields.Float('Sale', compute='get_desc3_sale')
    desc3_weight = fields.Float('Weight', default= 0.103,digits=(16,3))
    desc3_total = fields.Float('Total' , compute='get_desc3_total')

    @api.depends('type')
    def get_qty3(self):
        for rec in self:
            if rec.type in ('four_dalfa'):
                rec.desc3_qty = ((rec.w * 1.0) + (rec.h * 4.0)) * 1.10
            else:
                rec.desc3_qty = ((rec.w * 1.0) + (rec.h * 2.0)) * 1.10

    @api.depends('kilo_rate', 'desc3_weight')
    def get_desc3_purchase(self):
        for rec in self:
            rec.desc3_purchase = rec.desc3_weight * rec.kilo_rate

    @api.depends('margin', 'desc3_purchase')
    def get_desc3_sale(self):
        for rec in self:
            rec.desc3_sale = rec.margin * rec.desc3_purchase

    @api.depends('desc3_qty', 'desc3_sale')
    def get_desc3_total(self):
        for rec in self:
            rec.desc3_total = rec.desc3_qty * rec.desc3_sale

########################################################################################################################
    ###################################
    ###########    Tango   ############
    ###################################


    # line  4  ==>     Fly Screen Frame K 2206
    desc4 = fields.Char('Description', default='Fly Screen FRAME K 2206')
    desc4_qty = fields.Float('Qty', compute="get_qty4", digits=(16, 3))
    desc4_purchase = fields.Float('Purchase', compute='get_desc4_purchase', digits=(16, 3))
    desc4_sale = fields.Float('Sale', compute='get_desc4_sale', digits=(16, 3))
    desc4_weight = fields.Float('Weight', default=0.329, digits=(16, 3))
    desc4_total = fields.Float('Total', compute='get_desc4_total', digits=(16, 3))

    @api.depends('type')
    def get_qty4(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc4_qty = ((rec.w * 2.0) + (rec.h * 2.0)) * 1.10
            else:
                rec.desc4_qty = 0.0

    @api.depends('kilo_rate', 'desc4_weight')
    def get_desc4_purchase(self):
        for rec in self:
            rec.desc4_purchase = rec.desc4_weight * rec.kilo_rate

    @api.depends('margin', 'desc4_purchase')
    def get_desc4_sale(self):
        for rec in self:
            rec.desc4_sale = rec.margin * rec.desc4_purchase

    @api.depends('desc4_qty', 'desc4_sale')
    def get_desc4_total(self):
        for rec in self:
            rec.desc4_total = rec.desc4_qty * rec.desc4_sale

########################################################################################################################

    # line 5  ==>    Screen Sash Alu 007
    desc5 = fields.Char('Description', default='Screen Sash Alu 007')
    desc5_qty = fields.Float('Qty', compute="get_qty5", digits=(16, 3))
    desc5_purchase = fields.Float('Purchase', compute='get_desc5_purchase', digits=(16, 3))
    desc5_sale = fields.Float('Sale', compute='get_desc5_sale', digits=(16, 3))
    desc5_weight = fields.Float('Weight', default=0.581, digits=(16, 3))
    desc5_total = fields.Float('Total', compute='get_desc5_total', digits=(16, 3))

    @api.depends('type')
    def get_qty5(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc5_qty = ((rec.w * 2.0 * 0.65) + (rec.h * 2.0))  * 1.10
            elif rec.type in ('four_dalfa'):
                rec.desc5_qty = ((rec.w * 2.0) + (rec.h * 4.0))  * 1.10
            else:
                rec.desc5_qty = ((rec.w * 1.0) + (rec.h * 2.0)) * 1.10

    @api.depends('kilo_rate', 'desc5_weight')
    def get_desc5_purchase(self):
        for rec in self:
            rec.desc5_purchase = rec.desc5_weight * rec.kilo_rate

    @api.depends('margin', 'desc5_purchase')
    def get_desc5_sale(self):
        for rec in self:
            rec.desc5_sale = rec.margin * rec.desc5_purchase

    @api.depends('desc5_qty', 'desc5_sale')
    def get_desc5_total(self):
        for rec in self:
            rec.desc5_total = rec.desc5_qty * rec.desc5_sale

########################################################################################################################

    # line 6  ==>     Fly Screen Sash  Calypso K2281
    desc6 = fields.Char('Description', default='Fly Screen Sash  Calypso K2281')
    desc6_qty = fields.Float('Qty', compute="get_qty6", digits=(16, 3))
    desc6_purchase = fields.Float('Purchase', compute='get_desc6_purchase', digits=(16, 3))
    desc6_sale = fields.Float('Sale', compute='get_desc6_sale', digits=(16, 3))
    desc6_weight = fields.Float('Weight', default=0.103, digits=(16, 3))
    desc6_total = fields.Float('Total', compute='get_desc6_total', digits=(16, 3))

    @api.depends('type')
    def get_qty6(self):
        for rec in self:
            if rec.type in ('tree_dalfa'):
                rec.desc6_qty = ((rec.w * 2.0 * 0.65) + (rec.h * 2.0)) * 1.10
            elif rec.type in ('four_dalfa'):
                rec.desc6_qty = ((rec.w * 2.0) + (rec.h * 4.0)) * 1.10
            else:
                rec.desc6_qty = ((rec.w * 1.0) + (rec.h * 2.0)) * 1.10

    @api.depends('kilo_rate', 'desc6_weight')
    def get_desc6_purchase(self):
        for rec in self:
            rec.desc6_purchase = rec.desc6_weight * rec.kilo_rate

    @api.depends('margin', 'desc6_purchase')
    def get_desc6_sale(self):
        for rec in self:
            rec.desc6_sale = rec.margin * rec.desc6_purchase

    @api.depends('desc6_qty', 'desc6_sale')
    def get_desc6_total(self):
        for rec in self:
            rec.desc6_total = rec.desc6_qty * rec.desc6_sale

########################################################################################################################
    @api.depends('profile_types','desc1_total', 'desc2_total', 'desc3_total','desc4_total','desc5_total','desc6_total')
    def get_total(self):
        for rec in self:
            if rec.profile_types == 'jumbo':
                rec.total = rec.desc1_total + rec.desc2_total + rec.desc3_total
            elif rec.profile_types == 'tango':
                rec.total = rec.desc4_total + rec.desc5_total + rec.desc6_total
            else:
                rec.total = 0.0


########################################################################################################################