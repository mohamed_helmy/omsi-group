

from odoo import models,fields,api

class GlassPrice(models.Model):
    _name = 'glass.price'

    name = fields.Char()
    clear_glass_6 = fields.Float('شفاف 6 مم')
    clear_glass_5 = fields.Float('شفاف 5 مم')
    frosted_glass = fields.Float('مصنفر')
    fam_glass = fields.Float('فاميه')
    reflecting_glass = fields.Float('عاكس')
    double = fields.Float('مذدوج')

    security = fields.Float('سكيوريت')
    lamenety = fields.Float('ﻻمبينيتد')
    gorgian = fields.Float('جورجيان')
    security_lamenety = fields.Float('سكيوريت و ﻻمبينيتد')
    securit_gorgian = fields.Float('سكيوريت وجورجيان')
    gorgian_lamenety = fields.Float('جورجيان و ﻻمبينيتد')
    security_lamenety_gorgian = fields.Float('سكيوريت وجورجيان و ﻻمبينيتد')


