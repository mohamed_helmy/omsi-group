import oerplib
import time
import sys
import os
import base64

installed_modules=[

]
except_modules=[
]
modules=os.listdir('/mnt/extra-addons')
modules.extend(installed_modules)
modules = [x for x in modules if x not in except_modules]
url = sys.argv[1] if len(sys.argv) > 1 else 'web'
database_password = os.environ.get('DB_PASSWORD', 'admin')
database_name = os.environ.get('ODOO_DATABASE', 'default')
master_password = os.environ.get('MASTER_PASSWORD', "admin")
is_demo = True if os.environ.get('DEMO', '0') == 0 else False
print "Connect ", url
oerp = oerplib.OERP(server=url, protocol='xmlrpc', port=8069)
oerp.config['timeout'] = 60000
print "Get databases"
databases = oerp.db.list()
if database_name not in databases :
  if is_demo: 
    print "Create Database"
    oerp.db.create_database(master_password, database_name, True, 'en_US', database_password)
  else:
    print "restore backup"
    with open("backup.zip", "rb") as db_backup:
      db_base=base64.b64encode(db_backup.read())
      oerp.db.restore(master_password, database_name,db_base)
time.sleep(15)
databases = oerp.db.list()
print databases
for db in databases:
  print db +" login"
  oerp.login(user='admin', passwd=database_password, database=db)
  module_obj = oerp.get('ir.module.module')

  print "Update modules list"
  update_module_obj = oerp.get('base.module.update')
  updage_module_instance = update_module_obj.create({})
  update_module_obj.update_module(updage_module_instance)
  #Query_Modules From DB
  install_module_ids = module_obj.search([('name', 'in', modules),('state', '!=', 'installed')])
  upgrade_module_ids = module_obj.search([('name', 'in', modules),('state', '=', 'installed')])
  install_module_objs = module_obj.browse(installed_modules)
  upgrade_module_objs = module_obj.browse(upgrade_module_ids)

  if len(install_module_ids) > 0:
      print("Installing modules:")
      tries = 0
      while True:
          try:
              tries += 1
              module_obj.button_immediate_install(install_module_ids)
              for obj in install_module_objs:
                print "install module " + obj.name + " "+obj.installed_version
          except Exception as e:
              if tries > 3:
                  raise e
          else:
              break

  if len(upgrade_module_ids) > 0:
      print("upgrading modules:")
      tries = 0
      while True:
          try:
              tries += 1
              module_obj.button_immediate_upgrade(upgrade_module_ids)
              for obj in upgrade_module_objs:
                print "upgrade module " + obj.name + " "+obj.installed_version
          except Exception as e:
              if tries > 3:
                  raise e
          else:
              break
